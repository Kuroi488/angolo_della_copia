<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Cover_Type extends Model
{

    public static function sort( $id ){
        $types = DB::table( 'cover__types' )->where( 'associated-category', $id )->get();
        // ddd($types );
        return $types;
    }

    public static function storeNewType( $data ){
      $type = new Cover_Type;
      $type->name = $data[ 'name' ];
      $type[ 'associated-category' ] = $data[ 'category' ];
      $type->save();
    }

    public static function updateType( $data ){
      Cover_Type::where( 'id', $data[ 'id' ] )
              ->update( [
                  'name' => $data['name'],
                  'associated-category' => $data['category'],
                  'updated_at' => \Carbon\Carbon::now()
              ] );
    }

    public static function deleteType( $id ){
      $type = Cover_Type::find( $id );
      Cover::deleteFromType( $type[ 'associated-category' ], $id );
      Text_Color::deleteFromType( $type[ 'associated-category' ], $id );
      $type->delete();
    }

    public static function deleteFromCategory( $id ){
      $cover_types = Cover_Type::where( 'associated-category', $id )->get();
      foreach ($cover_types as $type ) {
        Cover_Type::deleteType( $type['id'] );
      }
    }

    /* Managing paths Methods */
    public static function createPath(){
      echo '/office/warehouse/covers-types/create';
    }
    public static function newPath(){
      return '/office/warehouse/covers-types/new';
    }
    public static function editPath( $id ){
      $token = csrf_token();
      echo '/office/warehouse/covers-types/edit' . '?' . 'id=' . $id . '&' . '_token=' . $token;
    }
    public static function updatePath( $id ){
      return '/office/warehouse/covers-types/update/' . $id;
    }
    public static function deletePath( $id ){
      $token = csrf_token();
      echo '/office/warehouse/covers-types/delete/' . '?' . 'id=' . $id . '&' . '_token=' . $token;
    }


}
