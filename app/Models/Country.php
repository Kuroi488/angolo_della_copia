<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
  public static function languages( $id ){
    $language = DB::table( 'languages' )
                                ->where( 'countryID', $id )
                                ->orderBy( 'name' )
                                ->get();

    return $language;
  }

  public static function storeNewCountry( $data ){
        $country = new Country;
        $country->name = $data['name'];
        $country->save();
  }

  public static function getCountry( $id ){
    $country = DB::table( 'countries' )
                ->where( 'id', $id )
                ->first();

    return $country;
  }

  public static function updateCountry( $data, $id ){

    function updateSingle( $data, $id ){
        Country::where( 'id', $id )
                ->update( [
                    'name' => $data['name'],
                    'updated_at' => \Carbon\Carbon::now()
                    ] );
    }

    function updateMultiple( $data, $id ){
        Country::where( 'id', $id )
                ->update( [
                    'name' => $data['name'],
                    'visible' => $data['visible'],
                    'updated_at' => \Carbon\Carbon::now()
                    ] );
        Language::updateChildLanguagesVisibility( $id, $data['visible'] );
    }

    if( isset($data['visible']) ){
      updateMultiple( $data, $id );
    }else{
      updateSingle( $data, $id );
    }

  }

  public static function deleteCountry( $id ){
    $country = Country::find( $id );
    Language::deleteChildLanguages( $id );
    $country->delete();
  }

  /* Managing paths Methods */
  public static function createPath(){
    echo '/office/data-entry/countries/create';
  }
  public static function newPath(){
    return '/office/data-entry/countries/new';
  }
  public static function editPath( $id ){
    echo '/office/data-entry/countries/edit/' . $id;
  }
  public static function updatePath( $id ){
    return '/office/data-entry/countries/update/' . $id;
  }
  public static function deletePath( $id ){
    $token = csrf_token();
    echo '/office/data-entry/countries/delete?' . 'id=' . $id . '&_token=' . $token;
  }
  public static function publishPath( $id ){
    echo '/office/data-entry/countries/publish/' . $id;
  }
  public static function unPublishPath( $id ){
    echo '/office/data-entry/countries/unpublish/' . $id;
  }

}
