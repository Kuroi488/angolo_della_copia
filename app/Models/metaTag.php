<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Model;

class metaTag extends Model
{
  public static function storeNewMeta( $permalink, $name, $content ){
      $metaTAG = new metaTag;
      $metaTAG->permalink = $permalink;
      $metaTAG->name = $name;
      $metaTAG->body = $content;
      $metaTAG->save();
  }

  public static function deleteOldMetas( $permalink ){
    $metas = metaTag::where( 'permalink', $permalink )
                ->get();
    foreach( $metas as $meta ){
      $meta->delete();
    }
  }

}
