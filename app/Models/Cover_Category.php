<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Cover_Type;

class Cover_Category extends Model
{
    public static function storeNewCategory( $data ){
      $category = new Cover_Category;
      $category->name                    = $data['name'];
      $category[ 'associated-country' ]  = $data['country'];
      $category[ 'associated-language' ] = $data['language'];
      $category->description             = $data['description'];
      $category->cost                    = $data['cost'];
      $category->save();
    }

    public static function updateCategory( $data ){
        
      Cover_Category::where( 'id', $data[ 'id' ] )
              ->update( [
                  'name'                => $data['name'],
                  'associated-country'  => $data['country'],
                  'associated-language' => $data[ 'language' ],
                  'description'         => $data[ 'description' ],
                  'cost'                => $data[ 'cost' ],
                  'updated_at'          => \Carbon\Carbon::now()
              ] );
    }

    public static function deleteCategory( $id ){
      Cover_Type::deleteFromCategory( $id );
      $category = Cover_Category::find( $id );
      $category->delete();
    }




    /* Managing paths Methods */
    public static function createPath(){
      echo '/office/warehouse/categories/create';
    }
    public static function newPath(){
      return '/office/warehouse/categories/new';
    }
    public static function editPath( $id ){
      $token = csrf_token();
      echo '/office/warehouse/categories/edit' . '?' . 'id=' . $id . '&' . '_token=' . $token;
    }
    public static function updatePath( $id ){
      return '/office/warehouse/categories/update/' . $id;
    }
    public static function deletePath( $id ){
      $token = csrf_token();
      echo '/office/warehouse/categories/delete/' . '?' . 'id=' . $id . '&' . '_token=' . $token;
    }


}
