<?php

namespace App\Models;
use DB;
use Illuminate\Http\RedirectResponse;
use Illuminate\Database\Eloquent\Model;

class Pages extends Model{

    public static function updateChildPagesVisibility( $countryID, $languageID, $visibility ){
      Pages::where( 'country', $countryID )
            ->where( 'language', $languageID )
            ->update([
              'visible'    => $visibility,
              'updated_at' => \Carbon\Carbon::now()
            ]);
    }

    public static function deleteChildPages( $countryID, $languageID ){
      $pages = Pages::where( 'country', $countryID )
            ->where( 'language', $languageID )
            ->get();
      if( $pages ){
        foreach ($pages as $page) {
          $permalink = $page->permalink;
          metaTag::deleteOldMetas( $permalink );
          Component::deleteComponent( $permalink );
          $page->delete();
        }
      }
    }

    public static function storeNewPage( $countryID, $languageID, $permalink ){
          $page = new Pages;
          $page->permalink = $permalink;
          $page->country   = $countryID;
          $page->language  = $languageID;
          $page->save();
    }

    public static function deletePage( $id ){
      $page = Pages::find( $id );
      $permalink = $page->permalink;
      metaTag::deleteOldMetas( $permalink );
      Component::deleteComponent( $permalink );
      $page->delete();
    }

    public static function updatePage( $countryID, $languageID, $permalink, $visibility, $id ){

      if( $visibility != null ){
        Pages::where( 'id', '=', $id )
              ->update([ 'country' => $countryID,
                         'language' => $languageID,
                         'permalink' => $permalink,
                         'visible' => $visibility
                      ]);
      }else{
        Pages::where( 'id', '=', $id )
              ->update([ 'country' => $countryID,
                         'language' => $languageID,
                         'permalink' => $permalink
                      ]);
      }
    }

    /* Managing paths Methods */
    public static function createPath(){
      echo '/office/data-entry/pages/create';
    }
    public static function newPath(){
      return '/office/data-entry/ajax/pages/new';
    }
    public static function editPath( $id ){
      echo '/office/data-entry/pages/edit/' . $id;
    }
    public static function updatePath( $id ){
      return '/office/data-entry/ajax/pages/update/' . $id;
    }
    public static function deletePath( $id ){
      $token = csrf_token();
      echo '/office/data-entry/pages/delete?' . 'id=' . $id . '&' . '_token=' . $token;
    }
    public static function publishPath(){
      $token = csrf_token();
      echo '/office/data-entry/pages/publish/' . '?' . '_token=' . $token;
    }
    public static function unPublishPath(){
      $token = csrf_token();
      echo '/office/data-entry/pages/unpublish/' . '?' . '_token=' . $token;
    }

}
