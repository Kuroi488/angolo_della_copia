<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use File;

class Text_Color extends Model
{
    public static function sort( $id ){
        $types = DB::table( 'text__colors' )->where( 'associated-category', $id )->get();
        return $types;
    }

    public static function colors( $category, $type ){
        $covers = DB::table( 'text__colors' )->where( [ [ 'associated-category', '=' , $category ],
                                                        [ 'associated-type', '=' , $type ]
                                                      ] )
                                            ->orderBy( 'name' )
                                            ->get();
        return $covers ? $covers : array();
    }

    public static function storeNewColor( $data ){
        $color = new Text_Color;
        $color->name                    = $data[ 'name' ];
        $color[ 'associated-category' ] = $data[ 'category' ];
        $color[ 'associated-type' ]     = $data[ 'type' ];
        $color[ 'background-image' ]    = $data[ 'preview' ];
        $color[ 'preview-image' ]       = $data[ 'icon' ];
        $color->save();
    }

    public static function updateColor( $data ){

        Text_Color::where( 'id', $data[ 'id' ] )
                    ->update( [ 'name'        => $data['name'],
                        'associated-category' => $data['category'],
                        'associated-type'     => $data['type'],
                        'updated_at' => \Carbon\Carbon::now()
                    ] );
        if( isset( $data[ 'icon' ] ) ){ Text_Color::where( 'id', $data[ 'id' ] )->update( [ 'preview-image' => $data[ 'icon' ] ] ); }
        if( isset( $data[ 'preview' ] ) ){ Text_Color::where( 'id', $data[ 'id' ] )->update( [ 'background-image' => $data[ 'preview' ] ] ); }
    }

    public static function deleteColor( $id ){
        $color = Text_color::find( $id );
        File::delete( public_path( 'assets/images/text_colors/' . $color[ 'background-image' ] ) );
        File::delete( public_path( 'assets/images/text_colors/' . $color[ 'preview-image' ] ) );
        $color->delete();
    }

    public static function deleteFromType( $categoryId, $typeId ){
        $colors = DB::table( 'text__colors' )->where(  [ 'associated-category' => $categoryId, 'associated-type' => $typeId ] )->get();
        foreach ( $colors as $color ) {
            self::deleteColor( $color->id );
        }
    }

    /* Managing paths Methods */
    public static function createPath(){
        echo '/office/warehouse/colors/create';
    }
    public static function newPath(){
        return '/office/warehouse/colors/store';
    }
    public static function editPath( $id ){
        $token = csrf_token();
        echo '/office/warehouse/colors/edit' . '?' . 'id=' . $id . '&' . '_token=' . $token;
    }
    public static function updatePath( $id ){
        return '/office/warehouse/colors/update/' . $id;
    }
    public static function deletePath( $id ){
        $token = csrf_token();
        echo '/office/warehouse/colors/delete/' . '?' . 'id=' . $id . '&' . '_token=' . $token;
    }
}
