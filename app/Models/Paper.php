<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Paper extends Model
{
    
    public static function deletePaper( $data ){
      $paper = Paper::find( $data[ 'id' ] );
      $paper->delete();
    }
    
    public static function storeNewPaper( $data ){
      $paper = new Paper;
      $paper[ 'associated-country' ]  = $data[ 'country' ];
      $paper[ 'associated-language' ] = $data[ 'language' ];
      $paper[ 'name' ]                = $data[ 'name' ];
      $paper[ 'cost' ]                = $data[ 'cost' ];
      $paper[ 'weight' ]              = $data[ 'weight' ];
      $paper->save();
    }
    
    public static function updatePaper( $data, $id ){
      
      Paper::where( 'id', '=', $id )
        ->update([ 'associated-country'  => $data[ 'country' ],
                   'associated-language' => $data[ 'language' ],
                   'name'                => $data[ 'name' ],
                   'cost'                => $data[ 'cost' ],
                   'weight'              => $data[ 'weight' ],
                   'updated_at'          => date("Y-m-d H:i:s")
        ]);
      
    }
    
    /* Managing paths Methods */

    public static function createPath(){
      echo '/office/warehouse/papers/create';
    }
    public static function newPath(){
      return '/office/warehouse/papers/new';
    }
    public static function editPath( $id ){
      echo '/office/warehouse/papers/edit/' . $id;
    }
    public static function updatePath( $id ){
      return '/office/warehouse/papers/update/' . $id;
    }
    public static function deletePath( $id ){
      $token = csrf_token();
      echo '/office/warehouse/papers/delete?' . 'id=' . $id . '&' . '_token=' . $token;
    }
    
}
