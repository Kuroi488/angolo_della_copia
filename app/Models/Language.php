<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;


class Language extends Model{

  public static function papers( $language, $country  ){
    $papers = DB::table( 'papers' )
                  ->where( [
                      [ 'associated-language', '=' , $language ],
                      [ 'associated-country', '=' , $country ]
                      ] )
                    ->get();
    return $papers;
  }

  public static function pages( $language, $country ){
    $pages = DB::table( 'pages' )
                    ->where( [
                      [ 'language', '=' , $language ],
                      [ 'country', '=' , $country ]
                      ] )
                    ->orderBy( 'permalink' )
                    ->get();
    return $pages;
  }

  public static function updateChildLanguagesVisibility( $countryID, $visibility ){
    $ids = Language::where( 'countryID', $countryID )->get();

    foreach ( $ids as $id ) {
      Language::where( 'countryID', $id->id )
              ->update([
                    'visible' => $visibility,
                    'updated_at' => \Carbon\Carbon::now()
                        ]);
      Pages::updateChildPagesVisibility( $countryID, $id->id, $visibility );
    }
  }

  public static function deleteChildLanguages( $countryID ){
    $languages = Language::where( 'countryID', $countryID )->get();

    if( $languages ){
      foreach ( $languages as $language ) {
        Pages::deleteChildPages( $countryID, $language->id );
        $language->delete();
      }
    }
  }

  public static function storeNewLanguage( $data ){
        $language = new Language;
        $language->name = $data['name'];
        $language->countryID = $data['country'];
        $language->save();
  }

  public static function updateLanguage( $data, $id ){

    function updateSingle( $data, $id ){
        Language::where( 'id', $id )
                ->update( [
                    'name' => $data['name'],
                    'countryID' => $data['country'],
                    'updated_at' => \Carbon\Carbon::now()
                    ] );
    }

    function updateMultiple( $data, $id ){
        Language::where( 'id', $id )
                ->update( [
                    'name' => $data['name'],
                    'countryID' => $data['country'],
                    'visible' => $data['visible'],
                    'updated_at' => \Carbon\Carbon::now()
                    ] );
        Pages::updateChildPagesVisibility( $data['country'], $id, $data['visible'] );
    }
      if( isset($data['visible']) ){
        updateMultiple( $data, $id );
      }else{
        updateSingle( $data, $id );
      }
  }

  public static function deleteLanguage( $id ){
    $language = Language::find( $id );
    Pages::deleteChildPages( $language->countryID, $id );
    $language->delete();
  }


  /* Managing paths Methods */
  public static function createPath(){
    echo '/office/data-entry/languages/create';
  }
  public static function newPath(){
    return '/office/data-entry/languages/new';
  }
  public static function editPath( $id ){
    echo '/office/data-entry/languages/edit/' . $id;
  }
  public static function updatePath( $id ){
    return '/office/data-entry/languages/update/' . $id;
  }
  public static function deletePath( $id ){
    $token = csrf_token();
    echo '/office/data-entry/languages/delete/' . '?' . 'id=' . $id . '&' . '_token=' . $token;
  }
  public static function publishPath( $id ){
    echo '/office/data-entry/languages/publish/' . $id;
  }
  public static function unPublishPath( $id ){
    echo '/office/data-entry/languages/unpublish/' . $id;
  }

}
