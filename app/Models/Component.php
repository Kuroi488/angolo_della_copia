<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Model;

class Component extends Model
{
  public static function storeNewComponent( $name, $index, $marker, $body, $permalink ){
        $component = new Component;
        $component->permalink = $permalink;
        $component->name = $name;
        $component->index = $index;
        $component->marker = $marker;
        $component->body = $body;
        $component->save();
  }

  public static function deleteComponent( $permalink ){
    $components = Component::where( 'permalink', $permalink )->get();
    foreach( $components as $component ){
      $component->delete();
    }
  }

}
