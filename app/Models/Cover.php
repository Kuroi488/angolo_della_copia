<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use File;

class Cover extends Model
{

  public static function sort( $id ){
      $types = DB::table( 'cover__types' )->where( 'associated-category', $id )->get();
      return $types;
  }

  public static function covers( $category, $type ){
    $covers = DB::table( 'covers' )->where( [ [ 'associated-category', '=' , $category ],
                                              [ 'associated-type', '=' , $type ]
                                            ] )
                                            ->orderBy( 'name' )
                                            ->get();
    return $covers ? $covers : array();
  }

  public static function storeNewCover( $data ){
    $cover = new Cover;
    $cover->name                    = $data[ 'name' ];
    $cover->quantity                = $data[ 'quantity' ];
    $cover[ 'associated-category' ] = $data[ 'category' ];
    $cover[ 'associated-type' ]     = $data[ 'type' ];
    $cover[ 'background-image' ]    = $data[ 'preview' ];
    $cover[ 'preview-image' ]       = $data[ 'icon' ];
    $cover->save();
  }

  public static function updateCover( $data ){

    Cover::where( 'id', $data[ 'id' ] )
            ->update( [ 'name'                => $data['name'],
                        'quantity'            => $data['quantity'],
                        'associated-category' => $data['category'],
                        'associated-type'     => $data['type'],
                        'updated_at' => \Carbon\Carbon::now()
                      ] );
    if( isset( $data[ 'icon' ] ) ){ Cover::where( 'id', $data[ 'id' ] )->update( [ 'preview-image' => $data[ 'icon' ] ] ); }
    if( isset( $data[ 'preview' ] ) ){ Cover::where( 'id', $data[ 'id' ] )->update( [ 'background-image' => $data[ 'preview' ] ] ); }
  }

  public static function deleteCover( $id ){
    $cover = Cover::find( $id );
    File::delete( public_path( 'assets/images/covers/' . $cover[ 'background-image' ] ) );
    File::delete( public_path( 'assets/images/covers/' . $cover[ 'preview-image' ] ) );
    $cover->delete();
  }

  public static function deleteFromType( $categoryId, $typeId ){
    $covers = DB::table( 'covers' )->where(  [ 'associated-category' => $categoryId, 'associated-type' => $typeId ] )->get();
    foreach ($covers as $cover ) {
      self::deleteCover( $cover->id );
    }
  }

  /* Managing paths Methods */
  public static function createPath(){
    echo '/office/warehouse/covers/create';
  }
  public static function newPath(){
    return '/office/warehouse/covers/store';
  }
  public static function editPath( $id ){
    $token = csrf_token();
    echo '/office/warehouse/covers/edit' . '?' . 'id=' . $id . '&' . '_token=' . $token;
  }
  public static function updatePath( $id ){
    return '/office/warehouse/covers/update/' . $id;
  }
  public static function deletePath( $id ){
    $token = csrf_token();
    echo '/office/warehouse/covers/delete/' . '?' . 'id=' . $id . '&' . '_token=' . $token;
  }

}
