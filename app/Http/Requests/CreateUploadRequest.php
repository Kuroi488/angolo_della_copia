<?php

namespace App\Http\Requests;
use App\Http\Requests\Request;

class CreateUploadRequest extends Request {

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' =>   'required_if:upload, file|mimes:pdf'
        ];
    }

    public function authorize()
    {
        return true;
    }

}
