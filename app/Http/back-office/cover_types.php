<?php
  Route::get( '/office/warehouse/covers-types/create', 'CoverTypesController@create' );
  Route::get( '/office/warehouse/covers-types/new', 'CoverTypesController@store' );
  Route::get( '/office/warehouse/covers-types/edit', 'CoverTypesController@edit' );
  Route::get( '/office/warehouse/covers-types/update/{id}', 'CoverTypesController@update' );
  Route::get( '/office/warehouse/covers-types/delete', 'CoverTypesController@deleteType' );
  
  Route::post( '/office/warehouse/ajax/types-list', 'CoverTypesController@typesIndex' );
