<?php
Route::get( '/office/warehouse/colors/create', 'TextColorController@create' );
Route::post( '/office/warehouse/colors/store', 'TextColorController@store' );
Route::get( '/office/warehouse/colors/edit', 'TextColorController@edit' );
Route::post( '/office/warehouse/colors/update/{id}', 'TextColorController@update' );
Route::get( '/office/warehouse/colors/delete', 'TextColorController@deleteColor' );
