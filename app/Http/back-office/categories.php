<?php
  Route::get( '/office/warehouse/categories/create', 'CategoryController@create' );
  Route::post( '/office/warehouse/categories/new', 'CategoryController@store' );
  Route::get( '/office/warehouse/categories/edit', 'CategoryController@edit' );
  Route::post( '/office/warehouse/categories/update/{id}', 'CategoryController@update' );
  Route::get( '/office/warehouse/categories/delete', 'CategoryController@deleteCategory' );

 ?>
