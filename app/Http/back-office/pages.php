<?php
Route::get( '/office/data-entry/pages/create', 'PagesController@create' );
Route::post( '/office/data-entry/ajax/languages-list-from-country', 'PagesController@languagesIndexfromCountry' );
Route::post( '/office/data-entry/ajax/pages/new', 'PagesController@newPage' );
Route::get( '/office/data-entry/pages/delete', 'PagesController@deletePage' );
Route::get( '/office/data-entry/pages/edit/{id}', 'PagesController@edit' );
Route::post( '/office/data-entry/ajax/pages/update/{id}', 'PagesController@update' );
?>
