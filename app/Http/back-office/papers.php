<?php
Route::get( '/office/warehouse/papers/create', 'PaperController@create' );
Route::get( '/office/warehouse/papers/new', 'PaperController@newPaper' );
Route::get( '/office/warehouse/papers/edit/{id}', 'PaperController@edit' );
Route::post( '/office/warehouse/papers/update/{id}', 'PaperController@update' );
Route::get( '/office/warehouse/papers/delete', 'PaperController@deletePaper' );
Route::get( '/ajax/returnPapersWeights', 'PaperController@getWeights' );
?>
