<?php
  Route::get( '/office/data-entry/countries/edit/{id}', 'CountryController@edit' );
  Route::get( '/office/data-entry/countries/update/{id}', 'CountryController@update' );
  Route::get( '/office/data-entry/countries/create', 'CountryController@create' );
  Route::get( '/office/data-entry/countries/new', 'CountryController@store' );
  Route::get( '/office/data-entry/countries/delete', 'CountryController@deleteCountry' );

 ?>
