<?php
Route::get( '/office/data-entry/languages/create', 'LanguageController@create' );
Route::get( '/office/data-entry/languages/new', 'LanguageController@store' );
Route::get( '/office/data-entry/languages/edit/{id}', 'LanguageController@edit' );
Route::get( '/office/data-entry/languages/update/{id}', 'LanguageController@update' );
Route::get( '/office/data-entry/languages/delete', 'LanguageController@deleteLanguage' );
?>
