<?php
  Route::get( '/office/warehouse/covers/create', 'CoversController@create' );
  Route::post( '/office/warehouse/covers/store', 'CoversController@store' );
  Route::get( '/office/warehouse/covers/edit', 'CoversController@edit' );
  Route::post( '/office/warehouse/covers/update/{id}', 'CoversController@update' );
  Route::get( '/office/warehouse/covers/delete', 'CoversController@deleteCover' );

 ?>
