<?php

use App\Models\Pages;
use App\libraries\DataEntry;

URL::forceSchema('https');


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get( '/tell_me_what_you_got', function(){
  phpinfo();
} );


/*session managing  */
require __DIR__ . '/front-end/sessionRoutes.php';

/* office routing */
Route::get( '/office', function(){
   return view( 'office/office_home' );
} );
Route::get( '/office/warehouse', function(){
  return view( 'office/warehouse' );
} );


/* data-entry index */
Route::get( '/office/data-entry/index', 'PagesController@index' );
// countries
require __DIR__ . '/back-office/countries.php';
// languages
require __DIR__ . '/back-office/languages.php';
// pages
require __DIR__ . '/back-office/pages.php';

Route::get( '/office/warehouse/covers/index', 'CoversController@index' );
// categories
require __DIR__ . '/back-office/categories.php';
// covers types
require __DIR__ . '/back-office/cover_types.php';
// covers
require __DIR__ . '/back-office/covers.php';
// papers
Route::get( '/office/warehouse/papers/index', 'PaperController@index' );
require __DIR__ . '/back-office/papers.php';
// text colors
Route::get( '/office/warehouse/colors/index', 'TextColorController@index' );
require __DIR__ . '/back-office/text_colors.php';

Route::get( '/ajax/returnCoversData', 'CoversController@returnCoversData' );



// front-office work around
Route::get( '/{any}', function( $any ){
    $page = DataEntry::index( $any );
    if ( $page == 404 ){
      return view( '404' );
    }
} )->where( 'any', '.*' );
