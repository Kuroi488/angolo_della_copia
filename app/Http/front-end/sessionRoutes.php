<?php

    Route::post( '/set_colorizer_config', function(){
        session_start();
        $data = Input::all();
        $_SESSION[ 'colorizer_config' ] = $data;
        print_r( ( null !== $_SESSION[ 'colorizer_config' ] ) ? $_SESSION[ 'colorizer_config' ] : 'no previuos configs' );
    });
    
    Route::post( '/get_colorizer_config', function(){
        $data = ( null !== $_SESSION[ 'colorizer_config' ] ) ? $_SESSION[ 'colorizer_config' ] : 'no previuos configs';
        return $data;
    });
    
    Route::post( '/set_order_config', function( Request $request ){
        $data = Input::all();
        session_start();
        if( !isset( $_SESSION[ 'order_config' ] ) ){
            $_SESSION['order_config'] = array();
        }
        foreach ( $data as $dataKey => $dataValue ) {
            $_SESSION['order_config'][ $dataKey ] = $dataValue;
        }
        print_r( $_SESSION[ 'order_config' ] );
    } );