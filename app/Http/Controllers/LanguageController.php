<?php

namespace App\Http\Controllers;
use DB;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\View\Middleware\ShareErrorsFromSession;
use Validator;
use Input;
use App\Models\Country;
use App\Models\Language;

class LanguageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::all();
        return view( 'office.pages.create-language' )->with( 'countries', $countries );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

      // custom rule
      Validator::extend( 'unique_for_country', function( $attribute, $value, $request ){

        $language_exists = DB::table( 'languages' )
                                    ->where( 'name', '=', $value )
                                    ->where( 'countryID', '=', Input::all()['country'] )
                                    ->get();

        return $language_exists ? false : true;

      } );
      $messages = array( 'name.unique_for_country' => 'The language already exists.', );

      $rules = array(
            'country' => 'required|numeric',
            'name' => 'required|min:2|max:5|unique_for_country',
            '_token' => 'required'
          );

      $validator = Validator::make( Input::all(), $rules, $messages );


      if ( $validator->fails() ){
        $errors = $validator->errors();
        return back()->with( 'errors', $errors );
      }else{
        Language::storeNewLanguage( Input::all() );
        return redirect( 'office/data-entry/index' );
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit($id){
         $language = Language::find( $id );
         $countries = Country::lists( 'name', 'id' );
        //  print_r( $language );
         return view( 'office.pages.edit-language' )->with( [ 'countries' => $countries, 'language' => $language ] );
     }

     public function update(Request $request, $id){
       $rules = array(
             'country' => 'required|numeric',
             'name' => 'required|max:5|min:2',
             '_token' => 'required',
           );

       $validator = Validator::make( Input::all(), $rules);

       if ( $validator->fails() ){
         $errors = $validator->errors();
         return back()->with( 'errors', $errors );
       }else{
         Language::updateLanguage( Input::all(), $id );
         return redirect( 'office/data-entry/index' );
       }

     }

     public static function deleteLanguage( Request $request ){
       $rules = array(
             'id' => 'required|max:5',
             '_token' => 'required',
           );

       $validator = Validator::make( Input::all(), $rules);

       if ( $validator->fails() ){
         $errors = $validator->errors();
         return back()->with( 'errors', $errors );
       }else{
         Language::deleteLanguage( Input::all()['id'] );
         return redirect( 'office/data-entry/index' );
       }
     }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
