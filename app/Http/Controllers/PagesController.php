<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\View\Middleware\ShareErrorsFromSession;
use Validator;
use Input;
use File;
use App\libraries\DataEntry;
use DB;

use App\Models\Pages;
use App\Models\Country;
use App\Models\Language;
use App\Models\Component;
use App\Models\metaTag;


class PagesController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index(){
		$countries = Country::orderBy('name', 'asc')->get();
		return view( 'office.pages.index')->with( 'countries', $countries );
	}

	public function create(){
		$countries = Country::all();
		$components = DataEntry::getComponents();
		return view( 'office.pages.create-page')->with( [ 'countries' => $countries, 'components' => $components ] );
	}

	public static function languagesIndexfromCountry( Request $request ){
		if( $request->header('X-CSRF-Token') ){
			$rules = array( 'id' => 'required', );
			$validator = Validator::make( Input::all(), $rules);
			if ( !$validator->fails() ){
				$list = Language::where( 'countryID', Input::all()['id'] )->lists( 'name', 'id' );
				return $list;
			}else{
				echo 'You shall not pass!!!';
			}
			// token protection
		}else{
			echo 'You shall not pass!!!';
		}
	}

	public function newPage( Request $request ){
		if( $request->header('X-CSRF-Token') ){

			// preparing all data
			$countryID = $request->input( 'countryID' );
			$languageID = $request->input( 'languageID' );
			$name 			= $request->input( 'name' );
			$metaTAGS		= $request->input( 'metaTAGS' );
			$components = $request->input( 'components' );
			$countryName 	= Country::where( 'id', $countryID )->first()->name;
			$languageName = Language::where( 'id', $languageID )->first()->name;
			$permalink = $countryName . '/' . $languageName . '/' . $name;
			$permalink = str_replace( ' ', '-', $permalink );
			$permalink = str_replace( '//', '/', $permalink );
			$permalink = str_replace( '\\', '', $permalink );
			define( "PERMALINK", $permalink );
			// custom rule
			Validator::extend( 'unique_for_country_and_language', function( $attribute, $value, $request ){
				$pageExists = DB::table( 'pages' )
													->where( 'permalink', '=', PERMALINK )
													->first();
				return $pageExists ? false : true;
			} );
			$messages = array( 'name.unique_for_country_and_language' => 'The page already exists.', );

			$rules = array( 'countryID'  => 'required|numeric',
											'languageID' => 'required|numeric',
											'name'			 => 'required|unique_for_country_and_language',
											'metaTAGS'	 => 'required|array',
											'components' => 'required|array'
										);

			$validator = Validator::make( $request->input(), $rules, $messages );

			if ( !$validator->fails() ){

				Pages::storeNewPage( $countryID, $languageID, $permalink );
				//metas
				foreach( $metaTAGS as $metaName => $metaContent ){
						metaTag::storeNewMeta( $permalink, $metaName, $metaContent );
				}
				//components
				foreach ( $components as $name => $marker ) {
					foreach ( $components[$name] as $marker => $body) {
						if( $marker != 'index' ){
							Component::storeNewComponent( $name, $components[$name]['index'], $marker, $body, $permalink );
						}
						if( $marker == 'index' && count( $marker == 1 ) ){
								Component::storeNewComponent( $name, $components[$name]['index'], 'null', 'null', $permalink );
						}
					}
				}

				echo 'ok';
			}else{
				$errors = $validator->errors();
				return $errors;
        // return back()->with( 'errors', $errors );
			}
		// token protection
		}else{
			echo 'You shall not pass!!!';
		}
	}

	public static function deletePage( Request $request ){
		$rules = array(
					'id' => 'required|max:5',
					'_token' => 'required',
				);

		$validator = Validator::make( Input::all(), $rules);

		if ( $validator->fails() ){
			$errors = $validator->errors();
			return back()->with( 'errors', $errors );
		}else{
			Pages::deletePage( Input::all()['id'] );
			return redirect( 'office/data-entry/index' );
		}
	}

	public static function edit( $id ){
		$languages = Language::get();
		$countries = Country::get();
		$page = Pages::where( 'id', '=', $id )->first();
		$metas = metaTag::where( 'permalink', $page->permalink )->get();
		$oldComponents = Component::where( 'permalink', '=', $page->permalink )->get();
		$usedComponents = dataEntry::makeComponentsArray( $oldComponents );
		$components = DataEntry::getComponents();
		return view( 'office.pages.edit-page' )->with([ 'countries' => $countries,
                                                         'languages' => $languages,
                                                         'metas' => $metas,
                                                         'usedComponents' => $usedComponents,
                                                         'page' => $page,
                                                         'components' => $components
                                                        ] );
	}

	public static function update( Request $request, $id ){
		if( $request->header('X-CSRF-Token') ){

			// preparing all data
			$oldPermalink = $request->input( 'oldPermalink' );
			$visibility = ( $request->input( 'visibility' ) ) ? $request->input( 'visibility' ) : null;
			$countryID = $request->input( 'countryID' );
			$languageID = $request->input( 'languageID' );
			$name 			= $request->input( 'name' );
			$metaTAGS		= $request->input( 'metaTAGS' );
			$components = $request->input( 'components' );
			$countryName 	= Country::where( 'id', $countryID )->first()->name;
			$languageName = Language::where( 'id', $languageID )->first()->name;
			$permalink = $countryName . '/' . $languageName . '/' . $name;
			$permalink = str_replace( ' ', '-', $permalink );
			$permalink = str_replace( '//', '/', $permalink );
			$permalink = str_replace( '\\', '', $permalink );
			define( "PERMALINK", $permalink );
			define( 'PAGEid', $id );

			Validator::extend( 'unique_for_country_and_language', function( $attribute, $value, $request ){
				$pageExists = DB::table( 'pages' )
													->where( 'id', '<>',  PAGEid )
													->where( 'permalink', PERMALINK )
													->get();
				return $pageExists ? false : true;
			} );
			$messages = array( 'name.unique_for_country_and_language' => 'The page already exists.' );

			$rules = array( 'countryID'    => 'required|numeric',
											'languageID'   => 'required|numeric',
											'name'			   => 'required|unique_for_country_and_language',
											'metaTAGS'	   => 'required|array',
											'components'   => 'required|array',
											'visibility'   => 'numeric',
											'oldPermalink' => 'required|string'
										);

			$validator = Validator::make( $request->input(), $rules, $messages );

			if ( !$validator->fails() ){
				//delete old metas and components
				metaTag::deleteOldMetas( $oldPermalink );
				Component::deleteComponent( $oldPermalink );
				Pages::updatePage( $countryID, $languageID, $permalink, $visibility, $id );
				//metas
				foreach( $metaTAGS as $metaName => $metaContent ){
						metaTag::storeNewMeta( $permalink, $metaName, $metaContent );
				}
				//components
				foreach ( $components as $name => $marker ) {
					foreach ( $components[$name] as $marker => $body) {

							if( $marker != 'index' ){
									Component::storeNewComponent( $name, $components[$name]['index'], $marker, $body, $permalink );
								}
							if( $marker == 'index' && count( $marker == 1 ) ){
									Component::storeNewComponent( $name, $components[$name]['index'], 'null', 'null', $permalink );
							}

					}
				}

				echo 'ok';

			}else{
				$errors = $validator->errors();
				return $errors;
			}
		// token protection
		}else{
			echo 'You shall not pass!!!';
		}
	}

}
