<?php

namespace App\Http\Controllers;
use App\Models\Text_Color;
use DB;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\View\Middleware\ShareErrorsFromSession;
use Validator;
use Input;
use App\Models\Cover_Category;
use App\Models\Cover_Type;
use App\Models\Cover;
use File;

class CoversController extends Controller
{

  public function index(){
   $categories = Cover_Category::orderBy('name', 'asc')->get();
   return view( 'office.covers.index')->with( 'categories', $categories );
 }

  public static function create(){
    $categories = Cover_Category::orderBy('name', 'asc')->get();
    return view( 'office/covers/create-cover' )->with( [ 'categories' => $categories ] );
  }

  public function store(Request $request){
    // custom rule
    Validator::extend( 'unique_for_covers', function( $attribute, $value, $request ){
      $type_exists = DB::table( 'covers' )
                                  ->where( 'name', '=', $value )
                                  ->where( 'associated-category', '=', Input::all()['category'] )
                                  ->where( 'associated-type', '=', Input::all()['type'] )
                                  ->get();
      return $type_exists ? false : true;
    } );

    $this->preview = isset( $request->preview ) ? $request->preview->getClientOriginalName() : false;
    $this->icon = isset( $request->icon ) ? $request->icon->getClientOriginalName() : false;

    Validator::extend( 'unique_image', function( $attribute, $value, $request ){

      $image_exists = DB::table( 'covers' )->where( 'background-image', '=', $this->preview )
                                           ->orWhere( 'preview-image',  '=', $this->icon )
                                           ->orWhere( 'background-image', '=', $this->icon )
                                           ->orWhere( 'preview-image', '=', $this->preview )
                                           ->get();
      return ( count( $image_exists ) > 0 ) ? false : true;
    } );
    $messages = array( 'name.unique_for_covers' => 'The cover already exists.',
                       'preview.unique_image' => 'The preview image name alredy exists',
                       'icon.unique_image'    => 'The icon image name alredy exists',
                     );
    $rules = array(
          'category' => 'required|numeric',
          'type'     => 'required|numeric',
          'name'     => 'required|min:2|unique_for_covers',
          'quantity' => 'required|numeric',
          'preview'  => 'required|unique_image|mimes:jpeg,bmp,png',
          'icon'     => 'required|unique_image|mimes:jpeg,bmp,png',
          '_token'   => 'required'
        );
    $validator = Validator::make( Input::all(), $rules, $messages );
    if ( $validator->fails() ){
      $errors = $validator->errors();
      return back()->with( 'errors', $errors );
    }else{

      $data = array();
      $data[ 'name' ]     = Input::all()[ 'name' ];
      $data[ 'quantity' ] = Input::all()[ 'quantity' ];
      $data[ 'category' ] = Input::all()[ 'category' ];
      $data[ 'type' ]     = Input::all()[ 'type' ];
      $data[ 'preview' ]  = $this->preview;
      $data[ 'icon' ]     = $this->icon;

      $request->preview->move( public_path( 'assets/images/covers' ), $this->preview );
      $request->icon->move( public_path( 'assets/images/covers' ), $this->icon );

      Cover::storeNewCover( $data );
      return redirect( 'office/warehouse/covers/index' );
    }
  }

   public function edit( Request $request ){
     $rules = array(
           'id' => 'required|numeric',
           '_token' => 'required',
         );
     $validator = Validator::make( Input::all(), $rules);
     if ( $validator->fails() ){
       $errors = $validator->errors();
       return back()->with( 'errors', $errors );
     }else{
       $cover = Cover::find( Input::all()[ 'id' ] );
       $categories = Cover_Category::orderBy('name', 'asc')->get();
       $types = Cover_Type::where( 'id', $cover[ 'associated-type' ] )->orderBy('name', 'asc')->get();
       return view( 'office/covers/edit-cover' )->with( [ 'cover' => $cover, 'categories' => $categories, 'types' => $types ] );
     }
   }

   public function update($id, Request $request ){
     // custom rule
     Validator::extend( 'unique_for_covers', function( $attribute, $value, $request ){
     $type_exists = DB::table( 'covers' )
                             ->where( 'id', '!=', Input::all()['id'] )
                             ->where( 'name', '=', $value )
                             ->where( 'associated-category', '=', Input::all()['category'] )
                             ->where( 'associated-type', '=', Input::all()['type'] )
                             ->get();
     return $type_exists ? false : true;
   } );

   $this->preview = isset( $request->preview ) ? $request->preview->getClientOriginalName() : false;
   $this->icon = isset( $request->icon ) ? $request->icon->getClientOriginalName() : false;

   Validator::extend( 'unique_image', function( $attribute, $value, $request ){
     $image_exists = DB::table( 'covers' )->where( 'id', '!=', Input::all()['id'] )
                                          ->where( 'background-image', '=', $this->preview )
                                          ->orWhere( 'preview-image', '=', $this->icon )
                                          ->orWhere( 'background-image', '=', $this->icon )
                                          ->orWhere( 'preview-image', '=', $this->preview )
                                          ->get();
                                          // ddd( $image_exists );
     return ( count( $image_exists ) > 0 ) ? false : true;
   } );
   $messages = array( 'name.unique_for_covers' => 'The cover already exists.',
                      'preview.unique_image' => 'The preview image name alredy exists',
                      'icon.unique_image'    => 'The icon image name alredy exists',
                    );

   $rules = array(
         'id'       => 'required|numeric',
         'category' => 'required|numeric',
         'type'     => 'required|numeric',
         'name'     => 'required|min:2|unique_for_covers',
         'quantity' => 'required|numeric',
         'preview'  => 'unique_image|mimes:jpeg,bmp,png,jpg',
         'icon'     => 'unique_image|mimes:jpeg,bmp,png,jpg',
         '_token'   => 'required'
       );
   $validator = Validator::make( Input::all(), $rules, $messages );
   if ( $validator->fails() ){
     $errors = $validator->errors();
     return back()->with( 'errors', $errors );
   }else{

     $data = array();
     $data[ 'id' ]       = Input::all()[ 'id' ];
     $data[ 'name' ]     = Input::all()[ 'name' ];
     $data[ 'quantity' ] = Input::all()[ 'quantity' ];
     $data[ 'category' ] = Input::all()[ 'category' ];
     $data[ 'type' ]     = Input::all()[ 'type' ];

     if( $request->preview ){
       $fileName = $request->preview->getClientOriginalName();
       $oldPreview = Cover::find( $data[ 'id' ] );
       $oldPreview = $oldPreview[ 'background-image' ];
       File::delete( public_path( 'assets/images/covers/' . $oldPreview ) );
       $request->preview->move( public_path( 'assets/images/covers' ), $fileName );
       $data[ 'preview' ] = $fileName;
     }

     if( $request->icon ){
       $fileName = $request->icon->getClientOriginalName();
       $oldIcon = Cover::find( $data[ 'id' ] );
       $oldIcon = $oldIcon[ 'preview-image' ];
       File::delete( public_path( 'assets/images/covers/' . $oldIcon ) );
       $request->icon->move( public_path( 'assets/images/covers' ), $fileName );
       $data[ 'icon' ] = $fileName;
     };


       Cover::updateCover( $data );
       return redirect( 'office/warehouse/covers/index' );
     }
   }

   public static function deleteCover( Request $request ){
     $rules = array(
           'id' => 'required',
           '_token' => 'required',
         );
     $validator = Validator::make( Input::all(), $rules);
     if ( $validator->fails() ){
       $errors = $validator->errors();
       return back()->with( 'errors', $errors );
     }else{
       Cover::deleteCover( Input::all()['id'] );
       return redirect( 'office/warehouse/covers/index' );
     }
   }
   
   public static function returnCoversData( Request $request ){
      $coversResponse = array();
       
       $categories = DB::table( 'cover__categories' )
                               ->select( 'cover__categories.id',
                                         'cover__categories.name',
                                         'cover__categories.cost',
                                         'cover__categories.description' )
                               ->join('countries', 'cover__categories.associated-country', '=', 'countries.id' )
                               ->join('languages', 'cover__categories.associated-language', '=', 'languages.id' )
                               ->where( 'countries.name', '=', $request->country)
                               ->where( 'languages.name', '=', $request->language)
                               ->get();
       
//       ddd( $categories );
           
      foreach( $categories as $category ){
          
        $tempCategory = array( 'name'        => $category->name,
                               'id'          => $category->id,
                               'cost'        => $category->cost,
                               'description' => $category->description,
                               'types'       => array()
                             );
          
        $types = Cover_Type::where( 'associated-category', $category->id )->get();
          
        foreach( $types as $type ){
          $tempType = array( 'name' => $type->name,
                             'id'   => $type->id,
                             'covers' => [],
                             'colors' => [] );
                             
          $covers = Cover::where( 'associated-category', '=', $category->id )
                            ->where( 'associated-type', '=', $type->id )
                            ->get();
            $colors = Text_Color::where( 'associated-category', '=', $category->id )
                ->where( 'associated-type', '=', $type->id )
                ->get();
                
          foreach( $covers as $cover ){
            
            $tempCover = array( 'id'      => $cover->id,
                                'name'    => $cover->name,
                                'preview' => $cover[ 'background-image' ],
                                'icon'    => $cover[ 'preview-image' ]
                              );
                              
            array_push( $tempType[ 'covers' ], $tempCover );
          }
            foreach( $colors as $color ){
                
                $tempColor = array( 'id'      => $color->id,
                                    'name'    => $color->name,
                                    'preview' => $color[ 'background-image' ],
                                    'icon'    => $color[ 'preview-image' ]
                                  );
                                  
                array_push( $tempType[ 'colors' ], $tempColor );
            }
            
          array_push( $tempCategory[ 'types' ], $tempType );
        }
        
        array_push( $coversResponse, $tempCategory );
      }
      
      echo json_encode( $coversResponse );
      
   }
   
}
