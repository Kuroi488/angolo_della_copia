<?php

namespace App\Http\Controllers;
use DB;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\View\Middleware\ShareErrorsFromSession;
use Validator;
use Input;
use App\Models\Cover_Category;
use App\Models\Cover_Type;
use App\Models\Cover;

class CoverTypesController extends Controller
{

    public static function typesIndex( Request $request ){
      if( $request->header('X-CSRF-Token') ){
        $rules = array(
              'id' => 'required|numeric',
            );
        $validator = Validator::make( Input::all(), $rules);
        if ( $validator->fails() ){
          $errors = $validator->errors();
          return $errors;
        }else{
          $cover_type = Cover_Type::where( 'associated-category', Input::all()['id'] )->lists( 'name', 'id' );
          return $cover_type ? $cover_type : 'err';
        }
    } else {
      echo 'You shall not pass!!';
    }
    }

    public static function create(){
      $categories = Cover_Category::orderBy('name', 'asc')->get();
      return view( 'office/covers/create-type' )->with( 'categories', $categories );
    }

    public function store(Request $request){

      // custom rule
      Validator::extend( 'unique_for_type', function( $attribute, $value, $request ){
        $type_exists = DB::table( 'cover__types' )
                                    ->where( 'name', '=', $value )
                                    ->where( 'associated-category', '=', Input::all()['category'] )
                                    ->get();
        return $type_exists ? false : true;
      } );
      $messages = array( 'name.unique_for_type' => 'The type already exists.', );
      $rules = array(
            'category' => 'required|numeric',
            'name' => 'required|min:5|unique_for_type',
            '_token' => 'required'
          );
      $validator = Validator::make( Input::all(), $rules, $messages );
      if ( $validator->fails() ){
        $errors = $validator->errors();
        return back()->with( 'errors', $errors );
      }else{
        Cover_Type::storeNewType( Input::all() );
        return redirect( 'office/warehouse/covers/index' );
      }
    }

     public function edit( Request $request ){
       $rules = array(
             'id' => 'required|numeric',
             '_token' => 'required',
           );
       $validator = Validator::make( Input::all(), $rules);
       if ( $validator->fails() ){
         $errors = $validator->errors();
         return back()->with( 'errors', $errors );
       }else{
         $cover_type = Cover_Type::find( Input::all()[ 'id' ] );
         $categories = Cover_Category::orderBy('name', 'asc')->get();
         return view( 'office/covers/edit-type' )->with( 'data', [ 'cover_type' => $cover_type, 'categories' => $categories ] );
       }
     }

     public function update($id, Request $request ){

       // custom rule
       Validator::extend( 'unique_for_type', function( $attribute, $value, $request ){
       $type_exists = DB::table( 'cover__types' )
                                   ->where( 'id', '!=', Input::all()['id'] )
                                   ->where( 'name', '=', $value )
                                   ->where( 'associated-category', '=', Input::all()['category'] )
                                   ->get();
       return $type_exists ? false : true;
     } );
     $messages = array( 'name.unique_for_type' => 'The type already exists.', );
     $rules = array(
           'id'       => 'required|numeric',
           'category' => 'required|numeric',
           'name'     => 'required|min:5|unique_for_type',
           '_token'   => 'required'
         );
     $validator = Validator::make( Input::all(), $rules, $messages );
       if ( $validator->fails() ){
         $errors = $validator->errors();
         return back()->with( 'errors', $errors );
       }else{
         Cover_Type::updateType( Input::all() );
         return redirect( 'office/warehouse/covers/index' );
       }
     }

     public static function deleteType( Request $request ){
       $rules = array(
             'id' => 'required|max:5',
             '_token' => 'required',
           );
       $validator = Validator::make( Input::all(), $rules);
       if ( $validator->fails() ){
         $errors = $validator->errors();
         return back()->with( 'errors', $errors );
       }else{
         Cover_Type::deleteType( Input::all()['id'] );
         return redirect( 'office/warehouse/covers/index' );
       }
     }
}
