<?php



namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Illuminate\View\Middleware\ShareErrorsFromSession;
use Validator;
use Input;
use App\Models\Country;
use App\Models\Language;
use App\Models\Paper;

class PaperController extends Controller
{
    public function getWeights(){
        
        $country = json_decode( json_encode( Country::where( 'name', '=', Input::all()[ 'country' ] )->get() ) );
        $language = json_decode( json_encode( Language::where( 'name', '=', Input::all()[ 'language' ] )->get() ) );
        
        $paperWeights = Paper::where([ [ 'associated-country', '=', $country[ 0 ]->id ],
                                       [ 'associated-language', '=', $language[ 0 ]->id ]
                                     ])->get();
        return json_encode( $paperWeights );
    }
    
    public function index(){
        $countries = Country::all();
        return view( 'office.papers.index')->with( 'countries', $countries );
    }
    
     public function create(){
		$countries = Country::all();
		return view( 'office.papers.create-paper')->with( [ 'countries' => $countries ] );
	}
	
	public static function languagesIndexfromCountry( Request $request ){
		if( $request->header('X-CSRF-Token') ){
			$rules = array( 'id' => 'required', );
			$validator = Validator::make( Input::all(), $rules);
			if ( !$validator->fails() ){
				$list = Language::where( 'countryID', Input::all()['id'] )->lists( 'name', 'id' );
				return $list;
			}else{
				echo 'You shall not pass!!!';
			}
			// token protection
		}else{
			echo 'You shall not pass!!!';
		}
	}
	
	public function newPaper( Request $request ){
			// preparing all data
			$country  = $request->input( 'country' );
			$language = $request->input( 'language' );
			$name 		= $request->input( 'name' );
			$cost		= $request->input( 'cost' );
			$weight 	= $request->input( 'weight' );
			$token 		= $request->input( '_token' );

            $rules = array( 'country'  => 'required|numeric',
    						'language' => 'required|numeric',
    						'cost'		 => 'required',
    						'weight'	 => 'required',
    						'_token'	 => 'required'
    					);

			$validator = Validator::make( $request->input(), $rules );

			if ( !$validator->fails() ){
				
				Paper::storeNewPaper( Input::all() );
				$countries = Country::all();
        		return view( 'office.papers.index')->with( 'countries', $countries );
				
			}else{
				$errors = $validator->errors();
                return back()->with( 'errors', $errors );
			}
		
	}
	
	public function edit($id){
         $paper = Paper::find( $id );
         $countries = Country::all();
         $languages = Language::all();
         return view( 'office.papers.edit-paper' )->with( [ 'countries' => $countries, 'languages' => $languages, 'paper' => $paper ] );
    }
    
    public function update(Request $request, $id){
       $rules = array(
             'country'	=> 'required|numeric',
             'language' => 'required|numeric',
             'name' 	=> 'required',
             '_token'	=> 'required',
             'cost' 	=> 'required',
             'weight'	=> 'required'
           );

       $validator = Validator::make( Input::all(), $rules);

       if ( $validator->fails() ){
         $errors = $validator->errors();
         return back()->with( 'errors', $errors );
       }else{
         Paper::updatePaper( Input::all(), $id );
         return redirect( 'office/warehouse/papers/index' );
       }

     }
     
     public function deletePaper(){
     	
     	 $rules = array(
             'id' 	  => 'required|numeric',
             '_token' => 'required',
           );

       $validator = Validator::make( Input::all(), $rules);

       if ( $validator->fails() ){
         $errors = $validator->errors();
         return back()->with( 'errors', $errors );
       }else{
         Paper::deletePaper( Input::all() );
         return redirect( 'office/warehouse/papers/index' );
       }
     	
     }
    
    
   
}
