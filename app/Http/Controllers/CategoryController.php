<?php

namespace App\Http\Controllers;
use DB;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\View\Middleware\ShareErrorsFromSession;
use Validator;
use Input;
use App\Models\Cover_Category;
use App\Models\Country;
use App\Models\Language;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public static function index(){
      $categories = Cover_Category::orderBy('name', 'asc')->get();
      return view( 'office.covers.index')->with( 'categories', $categories );
    }

    public function create(){
        $countries = Country::all();
        return view( 'office.covers.create-category' )->with( 'countries', $countries );
    }

    public function store(Request $request){

      $rules = array(
            'name'        => 'required|min:3',
            'country'     => 'required',
            'language'    => 'required',
            'description' => 'required',
            'cost'        => 'required|numeric|min:2',
            '_token'      => 'required'
          );

      $validator = Validator::make( Input::all(), $rules );
      if ( $validator->fails() ){
        $errors = $validator->errors();
        return back()->with( 'errors', $errors );
      }else{
        Cover_Category::storeNewCategory( Input::all() );
        return redirect( 'office/warehouse/covers/index' );
      }
    }

     public function edit( Request $request ){
         
       $rules = array(
             'id' => 'required|numeric',
             '_token' => 'required',
           );

       $validator = Validator::make( Input::all(), $rules);
       if ( $validator->fails() ){
         $errors = $validator->errors();
         return back()->with( 'errors', $errors );
       }else{
           
          $category = Cover_Category::find( $request->id );
          $countries = Country::all();
          $languages = Language::all();
          return view( 'office/covers/edit-category' )->with( [ 'category' => $category,
                                                                'countries' => $countries,
                                                                'languages' => $languages
                                                              ] );
       }

     }

     public function update($id, Request $request ){

       $rules = array(
            'id'          => 'required|numeric',
            'cost'        => 'required|numeric',
            'country'     => 'required',
            'language'    => 'required',
            'description' => 'required',
            'name'        => 'required|min:2',
            '_token'      => 'required'
           );

       $validator = Validator::make( Input::all(), $rules);

       if ( $validator->fails() ){
         $errors = $validator->errors();
         return back()->with( 'errors', $errors );
       }else{
         Cover_Category::updateCategory( Input::all() );
         return redirect( 'office/warehouse/covers/index' );
       }

     }

     public static function deleteCategory( Request $request ){
       $rules = array(
             'id' => 'required|max:5',
             '_token' => 'required',
           );

       $validator = Validator::make( Input::all(), $rules);

       if ( $validator->fails() ){
         $errors = $validator->errors();
         return back()->with( 'errors', $errors );
       }else{
         Cover_Category::deleteCategory( Input::all()['id'] );
         return redirect( 'office/warehouse/covers/index' );
       }
     }
}
