<?php

namespace App\Http\Controllers;
use DB;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\View\Middleware\ShareErrorsFromSession;
use Validator;
use Input;
use App\Models\Cover_Category;
use App\Models\Cover_Type;
use App\Models\Text_Color;
use File;

class TextColorController extends Controller
{

    public function index(){
        $categories = Cover_Category::orderBy('name', 'asc')->get();
        return view( 'office.colors.index')->with( 'categories', $categories );
    }

    public static function create(){
        $categories = Cover_Category::orderBy('name', 'asc')->get();
        return view( 'office/colors/create-color' )->with( [ 'categories' => $categories ] );
    }

    public function store(Request $request){
        // custom rule

        $this->preview = isset( $request->preview ) ? $request->preview->getClientOriginalName() : false;
        $this->icon = isset( $request->icon ) ? $request->icon->getClientOriginalName() : false;

        Validator::extend( 'unique_image', function( $attribute, $value, $request ){

            $image_exists = DB::table( 'text__colors' )->where( 'background-image', '=', $this->preview )
                ->orWhere( 'preview-image',  '=', $this->icon )
                ->orWhere( 'background-image', '=', $this->icon )
                ->orWhere( 'preview-image', '=', $this->preview )
                ->get();
            return ( count( $image_exists ) > 0 ) ? false : true;
        } );
        $messages = array( 'preview.unique_image' => 'The preview image name alredy exists',
                           'icon.unique_image'    => 'The icon image name alredy exists',
                          );
        $rules = array(
            'category' => 'required|numeric',
            'type'     => 'required|numeric',
            'name'     => 'required|min:5',
            'preview'  => 'required|unique_image|mimes:jpeg,bmp,png',
            'icon'     => 'required|unique_image|mimes:jpeg,bmp,png',
            '_token'   => 'required'
        );
        $validator = Validator::make( Input::all(), $rules, $messages );
        if ( $validator->fails() ){
            $errors = $validator->errors();
            return back()->with( 'errors', $errors );
        }else{

            $data = array();
            $data[ 'name' ]     = Input::all()[ 'name' ];
            $data[ 'category' ] = Input::all()[ 'category' ];
            $data[ 'type' ]     = Input::all()[ 'type' ];
            $data[ 'preview' ]  = $this->preview;
            $data[ 'icon' ]     = $this->icon;

            $request->preview->move( public_path( 'assets/images/text_colors' ), $this->preview );
            $request->icon->move( public_path( 'assets/images/text_colors' ), $this->icon );

            Text_Color::storeNewColor( $data );
            return redirect( 'office/warehouse/colors/index' );
        }
    }

    public function edit( Request $request ){
        $rules = array(
            'id' => 'required|numeric',
            '_token' => 'required',
        );
        $validator = Validator::make( Input::all(), $rules);
        if ( $validator->fails() ){
            $errors = $validator->errors();
            return back()->with( 'errors', $errors );
        }else{
            $color = Text_Color::find( Input::all()[ 'id' ] );
            $categories = Cover_Category::orderBy('name', 'asc')->get();
            $types = Cover_Type::where( 'id', $color[ 'associated-type' ] )->orderBy('name', 'asc')->get();
            return view( 'office/colors/edit-color' )->with( [ 'color' => $color, 'categories' => $categories, 'types' => $types ] );
        }
    }

    public function update($id, Request $request ){
        // custom rule

        $this->preview = isset( $request->preview ) ? $request->preview->getClientOriginalName() : false;
        $this->icon = isset( $request->icon ) ? $request->icon->getClientOriginalName() : false;

        Validator::extend( 'unique_image', function( $attribute, $value, $request ){
            $image_exists = DB::table( 'text__colors' )->where( 'id', '!=', Input::all()['id'] )
                ->where( 'background-image', '=', $this->preview )
                ->orWhere( 'preview-image', '=', $this->icon )
                ->orWhere( 'background-image', '=', $this->icon )
                ->orWhere( 'preview-image', '=', $this->preview )
                ->get();
            // ddd( $image_exists );
            return ( count( $image_exists ) > 0 ) ? false : true;
        } );
        $messages = array( 'preview.unique_image' => 'The preview image name alredy exists',
                           'icon.unique_image'    => 'The icon image name alredy exists',
                         );

        $rules = array(
            'id'       => 'required|numeric',
            'category' => 'required|numeric',
            'type'     => 'required|numeric',
            'name'     => 'required|min:5',
            'preview'  => 'unique_image|mimes:jpeg,bmp,png,jpg',
            'icon'     => 'unique_image|mimes:jpeg,bmp,png,jpg',
            '_token'   => 'required'
        );
        $validator = Validator::make( Input::all(), $rules, $messages );
        if ( $validator->fails() ){
            $errors = $validator->errors();
            return back()->with( 'errors', $errors );
        }else{

            $data = array();
            $data[ 'id' ]       = Input::all()[ 'id' ];
            $data[ 'name' ]     = Input::all()[ 'name' ];
            $data[ 'category' ] = Input::all()[ 'category' ];
            $data[ 'type' ]     = Input::all()[ 'type' ];

            if( $request->preview ){
                $fileName = $request->preview->getClientOriginalName();
                $oldPreview = Text_Color::find( $data[ 'id' ] );
                $oldPreview = $oldPreview[ 'background-image' ];
                File::delete( public_path( 'assets/images/text_colors/' . $oldPreview ) );
                $request->preview->move( public_path( 'assets/images/text_colors' ), $fileName );
                $data[ 'preview' ] = $fileName;
            }

            if( $request->icon ){
                $fileName = $request->icon->getClientOriginalName();
                $oldIcon = Text_Color::find( $data[ 'id' ] );
                $oldIcon = $oldIcon[ 'preview-image' ];
                File::delete( public_path( 'assets/images/text_colors/' . $oldIcon ) );
                $request->icon->move( public_path( 'assets/images/text_colors' ), $fileName );
                $data[ 'icon' ] = $fileName;
            };


            Text_Color::updateColor( $data );
            return redirect( 'office/warehouse/colors/index' );
        }
    }

    public static function deleteColor( Request $request ){
        $rules = array(
            'id' => 'required',
            '_token' => 'required',
        );
        $validator = Validator::make( Input::all(), $rules);
        if ( $validator->fails() ){
            $errors = $validator->errors();
            return back()->with( 'errors', $errors );
        }else{
            Text_Color::deleteColor( Input::all()['id'] );
            return redirect( 'office/warehouse/colors/index' );
        }
    }

}
