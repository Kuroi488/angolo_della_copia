<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Country;
use Illuminate\View\Middleware\ShareErrorsFromSession;
use Validator;
use Input;

class CountryController extends Controller
{

    public function create()
    {
        return view( 'office.pages.create-country' );
    }

    public function store(Request $request){

      $rules = array(
            'name' => 'required|max:5|min:2|unique:countries',
            '_token' => 'required',
          );

      $validator = Validator::make( Input::all(), $rules);

      if ( $validator->fails() ){
        $errors = $validator->errors();
        return back()->with( 'errors', $errors );
      }else{
        Country::storeNewCountry( Input::all() );
        return redirect( 'office/data-entry/index' );
      }
    }

    public function edit($id){
        $country = Country::getCountry( $id );
        return view( 'office.pages.edit-country' )->with( 'country', $country );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
      $rules = array(
            'name' => 'required|max:5',
            '_token' => 'required',
          );

      $validator = Validator::make( Input::all(), $rules);

      if ( $validator->fails() ){
        $errors = $validator->errors();
        return back()->with( 'errors', $errors );
      }else{
        Country::updateCountry( Input::all(), $id );
        return redirect( 'office/data-entry/index' );
      }

    }

    public static function deleteCountry( Request $request ){
      $rules = array(
            'id' => 'required|max:5',
            '_token' => 'required',
          );

      $validator = Validator::make( Input::all(), $rules);

      if ( $validator->fails() ){
        $errors = $validator->errors();
        return back()->with( 'errors', $errors );
      }else{
        Country::deleteCountry( Input::all()['id'] );
        return redirect( 'office/data-entry/index' );
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
