<?php

namespace App\libraries;

class BreadCrumbs{

/*get url and clean it from get calls*/
  private function url(){
    $full_url = $_SERVER['REQUEST_URI'];
    $full_url = strtok( $full_url, '?' );

    /* clean array from dashes */
    $full_url = str_replace( '-', ' ', $full_url );

    return $full_url;
  }

/* compose array in order to create the list */
  private function url_array(){
    $url = $this->url();
    $url_array = explode( '/', $url );
    /* remove first empty element of array */
    $url_array = array_splice( $url_array, 1 );
    return $url_array;
  }



/* composes the ordinate bootstrap list */
  private function compose_list(){

    $elements = $this->url_array();
    $urls = $this->url();

    /* list open tag */
    $list = '<ol class="breadcrumb">';

    foreach ($elements as $value) {

      /* create path for middle link */
      $path = explode( $value, $urls );
      $path = $path[0] . '' . $value . '';
      /* reinsert dashes where needed */
      $path = str_replace( ' ', '-', $path );

      /* get first element of list */
      if($value === reset($elements)){
        $list = $list . '<li><a href="/' . $value . '">Home</a></li>';
      }

      /* get last element of list */
      if($value === end($elements)){
          $list = $list . '<li class="active">' . $value . '</li>';
      }

      /* get middle elements of list */
      if($value != end($elements) && $value != reset($elements)){
        $list = $list . '<li><a href="' . $path . '">' . $value . '</a></li>';
      }

    }

    /* list close tag */
    $list = $list . '</ol>';

    /* if it's index page, do not print */
    if( count( $elements ) > 1 ){
      return $list;
    }else{
      return '';
    }
  }

/*method to get the composed list and print it*/
  public function print_breadCrumb(){
    echo $this->compose_list();
  }

  /* mothod to return correct rgb color */
  public function rePrintColor( $color ){

    $r = '';
    $g = '';
    $b = '';

    if( strlen( $color) == 9 ){

      for( $i = 0; $i < strlen( $color ); $i++ ){

        if( $i <= 2 ){
          $r .= $color[$i];
        }
        if( $i > 2 && $i <= 5 ){
          $g .= $color[$i];
        }
        if( $i > 5 ){
          $b .= $color[$i];
        }

      }

        echo '<div class="col-md-6 col-md-offset-3" style="background-color: rgb(' . $r . ',' . $b . ',' . $b . '); height: 30px;"></div>';
    }
  }

}
