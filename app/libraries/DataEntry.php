<?php

namespace App\libraries;

use File;
use DB;
Use App\Models\Pages;
Use App\Models\Component;
Use App\Models\metaTag;

class DataEntry{

  //manage custom routing
  public static function index( $path ){
    $page = DB::table('pages')->where('permalink', $path)->first();
    if ( $page ) {
      self::buildPage( $path );
    } else {
      return 404;
    }
  }

  public static function getMarkers( $file ){
      $elements = [];
      $file = glob( $file . '/' . '*.html' )[0];
      $contents = File::get($file);
      $section = new \DOMDocument();
      libxml_use_internal_errors(true);
      $section->loadHTML($contents);
      foreach( $section->getElementsByTagName('*') as $element ){
        $value = $element->getAttribute('data-entry');
        if( $value ){ array_push( $elements, $value ); }
      }
      libxml_clear_errors();
      return $elements;
  }

  public static function getComponents(){
    $components = File::directories( resource_path('components') );
		$componentsCleanList = [];
		foreach ($components as $component) {
			if( !strpos( $component, 'main' ) ){
        $elements = self::getMarkers( $component );
        // clean string according to environment
        if (strpos($component, '\\') !== false) {
          $component = explode("\\", $component);
        }else{
          $component = explode("/", $component);
        }
  			$component = end( $component );
  			$component = [ 'pageName' => $component, 'elements' => $elements ];
  			array_push($componentsCleanList, $component);
      }
		}
    return $componentsCleanList;
  }

  public static function makeComponentsArray( $usedComponents ){
    $newComponentsArray = array();
    $name = '';
    $index = -1;
    $componentStructure = array();
    foreach ( $usedComponents as $usedComponent ) {
      if( $usedComponent->index != $index ){
        $index = $usedComponent->index;
        $name = $usedComponent->name;
        $newComponentsArray[ $name ] = array();
      }
        array_push( $newComponentsArray[ $name ],
                          [ 'marker' => $usedComponent->marker,
                            'body' => $usedComponent->body
                          ] );
    }
    return $newComponentsArray;
  }

//build webview parts
  public static function buildHead( $permalink ){
    $openHeader = '<head>';
    $bodyHeader = '';
    $bodyHeader .= '<meta charset="UTF-8">';
    $bodyHeader .= '<meta name="viewport" content="width=device-width, initial-scale=1.0">';
    //common styles and scripts
    $bodyHeader .= '<link rel="stylesheet" type="text/css" href="' . asset( 'assets/css/bootstrap.min.css' ) . '">';
    $bodyHeader .= '<link rel="stylesheet" type="text/css" href="' . asset( 'assets/css/bootstrap-theme.min.css' ) . '">';
    $bodyHeader .= '<link rel="stylesheet" type="text/css" href="' . asset( 'assets/css/font-awesome.min.css' ) . '">';
    $bodyHeader .= '<link rel="stylesheet" type="text/css" href="' . asset( 'assets/css/jquery-ui.min.css' ) . '">';
    $bodyHeader .= '<link rel="stylesheet" type="text/css" href="' . asset( 'assets/frontend/css/main.css' ) . '">';
    $bodyHeader .= '<link rel="stylesheet" type="text/css" href="' . asset( 'assets/frontend/css/cropper.css' ) . '">';
    $bodyHeader .= '<script type="text/javascript" src="' . asset( 'assets/js/jquery.js' ) . '"></script>';
    $bodyHeader .= '<script type="text/javascript" src="' . asset( 'assets/js/jquery-ui.min.js' ) . '"></script>';
    $bodyHeader .= '<script type="text/javascript" src="' . asset( 'assets/js/bootstrap.min.js' ) . '"></script>';
    // $bodyHeader .= '<script type="text/javascript" src="' . asset( 'assets/js/vendor/angular/angular.min.js' ) . '"></script>';
    $metas = metaTag::where( 'permalink', $permalink )->get();
    foreach ($metas as $meta) {
      $name = strstr( $meta['name'], '-', true );
      $bodyHeader .= '<meta name="' . $name . '" content="' . $meta->body . '">';
    }
    $closeHeader = '</head>';
    return $openHeader . $bodyHeader . $closeHeader;
  }

  public static function createMenu( $path ){
    $language = [ 'country' => explode( '/', $path )[0] , 'language' => explode( '/', $path )[1] ];
    return view( 'front_end' )->with( 'language', $language );
  }

  public static function createFooter( $path ){
    $language = [ 'country' => explode( '/', $path )[0] , 'language' => explode( '/', $path )[1] ];
    return view( 'front_end_footer' )->with( 'language', $language );
  }

  public static function buildContent( $path ){
    $openBody = '<body>';
    $menu = self::createMenu( $path );
    $content = '';
    $components = Component::where( 'permalink', $path )->get();
    $components = self::makeComponentsArray( $components );
    $componentsDirectory = resource_path('components');
    foreach ( $components as $name => $componentContent ) {
      $name = strstr( $name, '@#', true );
      $name = str_replace( ' ', '', $name );
        $file = glob( $componentsDirectory . '/' . $name . '/' . '*.html' );
        if( !empty($file) ){
          $file = $file[0];
        $html = file_get_contents( $file , true);
        foreach( $componentContent as $element ){
          $html = str_replace( '##' . $element['marker'] . '##', '' . $element['body'] . '', $html );
        }
        $content .= $html;
      }
    }

    $content .= self::createFooter( $path );

    $closeBody = '</body>';
    return $openBody . $menu . $content . $closeBody;

  }

  public static function buildPage( $path ) {
    $openHTML = '<!DOCTYPE><html>';
    $head = self::buildHead( $path );
    $body = self::buildContent( $path );
    $closeHTML = '</html>';

    echo $openHTML . $head . $body . $closeHTML;
  }

  public static function bailString( $array, $string, $type ){
    if( $type == 'link' ){
      $check = array_key_exists( $string, $array['links'] );
      $content = ( $check && strlen( $array['links'][$string] ) > 0 ) ? $array['links'][$string] : '#';
    }else{
      $check = array_key_exists( $string, $array );
      $content = ( $check && strlen( $array[$string] ) > 0 ) ? $array[$string] : 'Lorem Ipsum';
    }
    return $content;
  }
  
  public static function getActiveString( $array, $string ){
    $linkToTest = self::bailString( $array, $string, 'link' );
    return ( strpos( $_SERVER['REQUEST_URI'], $linkToTest ) ) ? 'active' : '';
  }
  

}
