<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTextColorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('text__colors', function (Blueprint $table) {
          $table->increments('id');
          $table->string( 'name' );
          $table->string( 'background-image' );
          $table->string( 'preview-image' );
          $table->string( 'rgb', 9 );
          $table->integer( 'associated-category' );
          $table->integer( 'associated-type' );
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('text__colors');
    }
}
