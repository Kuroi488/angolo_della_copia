<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('covers', function (Blueprint $table) {
            $table->increments('id');
            $table->string( 'name' );
            $table->string( 'background-image' );
            $table->string( 'preview-image' );
            $table->boolean( 'avaible' );
            $table->integer( 'quantity' );
            $table->integer( 'associated-category' );
            $table->integer( 'associated-type' );
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('covers');
    }
}
