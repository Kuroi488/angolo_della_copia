-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generato il: Ago 02, 2017 alle 23:13
-- Versione del server: 5.5.55-0ubuntu0.14.04.1
-- Versione PHP: 5.6.30-10+deb.sury.org~trusty+2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `angolo_della_copia`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `components`
--

CREATE TABLE IF NOT EXISTS `components` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permalink` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `index` int(11) NOT NULL,
  `marker` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` varchar(10000) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=40 ;

--
-- Dump dei dati per la tabella `components`
--

INSERT INTO `components` (`id`, `permalink`, `name`, `index`, `marker`, `body`, `created_at`, `updated_at`) VALUES
(38, 'it/it/colorizer', ' angular-book-bindings @#0', 0, 'editoriale Titolo', '<h1 style="text-align: center; "><b><i>LE NOSTRE CATEGORIE</i></b></h1>', '2017-07-04 10:12:38', '2017-07-04 10:12:38'),
(39, 'it/it/colorizer', ' angular-book-bindings @#0', 0, 'null', 'null', '2017-07-04 10:12:38', '2017-07-04 10:12:38');

-- --------------------------------------------------------

--
-- Struttura della tabella `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `visible` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dump dei dati per la tabella `countries`
--

INSERT INTO `countries` (`id`, `name`, `visible`, `created_at`, `updated_at`) VALUES
(1, 'it', 0, '2017-06-08 19:22:44', '2017-06-08 19:22:44');

-- --------------------------------------------------------

--
-- Struttura della tabella `covers`
--

CREATE TABLE IF NOT EXISTS `covers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `background-image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `preview-image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avaible` tinyint(1) NOT NULL,
  `quantity` int(11) NOT NULL,
  `associated-category` int(11) NOT NULL,
  `associated-type` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Dump dei dati per la tabella `covers`
--

INSERT INTO `covers` (`id`, `name`, `background-image`, `preview-image`, `avaible`, `quantity`, `associated-category`, `associated-type`, `created_at`, `updated_at`) VALUES
(1, 'Rosso', 'rigida - similpelle - rosso.jpg', 'similpelle_rosso_text.png', 0, 10, 8, 16, '2017-06-24 19:16:41', '2017-06-24 19:16:41'),
(2, 'Bordeaux', 'rigida - similpelle - bordeaux.jpg', 'similpelle_bordeaux_text.png', 0, 10, 8, 16, '2017-06-24 19:18:20', '2017-06-24 19:18:20'),
(3, 'Verde scuro', 'rigide - similpelle - verde scuro.jpg', 'similpelle_verdescuro_text.png', 0, 10, 8, 16, '2017-06-24 19:19:19', '2017-06-24 19:19:19'),
(4, 'Blu scuro', 'rigida - similpelle - blu scuro.jpg', 'similpelle_bluscuro_text.png', 0, 10, 8, 16, '2017-06-24 19:20:24', '2017-06-24 19:20:24'),
(5, 'Beige', 'img_anteprima_cover_deluxe_telato_beige.jpg', 'deluxetelato_beige_text.png', 0, 10, 8, 17, '2017-06-24 19:22:02', '2017-06-24 19:22:02'),
(6, 'Bordeaux', 'img_anteprima_cover_deluxe_telato_bordeaux.jpg', 'deluxetelato_bordeaux_text.png', 0, 10, 8, 17, '2017-06-24 19:22:57', '2017-06-24 19:22:57'),
(7, 'Blu --', 'img_anteprima_cover_deluxe_telato_blu.jpg', 'deluxetelato_blu_text.png', 0, 10, 8, 17, '2017-06-24 19:29:54', '2017-06-24 19:29:54'),
(8, 'nero 5', 'img_anteprima_cover_prestige_black_satin.jpg', 'deluxesatinato_nero_text.png', 0, 10, 8, 18, '2017-06-24 19:32:35', '2017-06-24 19:32:35'),
(9, 'Bordeaux', 'img_anteprima_cover_prestige_bordeaux_satin.jpg', 'deluxesatinato_bordeaux_text.png', 0, 10, 8, 18, '2017-06-24 19:33:25', '2017-06-24 19:33:25'),
(10, 'Verde scuro', 'img_anteprima_cover_prestige_verdescuro_satin.jpg', 'deluxesatinato_verdescuro_text.png', 0, 10, 8, 18, '2017-06-24 19:34:13', '2017-06-24 19:34:13'),
(11, 'Blu scuro', 'img_anteprima_cover_prestige_bluscuro_satin.jpg', 'deluxesatinato_bluscuro_text.png', 0, 10, 8, 18, '2017-06-24 19:35:01', '2017-06-24 19:35:01'),
(12, 'Bianco', 'img_anteprima_cover_deluxe_met_bianco.jpg', 'deluxemetallico_bianco_text.png', 0, 10, 8, 19, '2017-06-24 19:35:40', '2017-06-24 19:35:40'),
(13, 'Azzurro', 'img_anteprima_cover_deluxe_met_azzurro.jpg', 'deluxemetallico_azzurro_text.png', 0, 10, 8, 19, '2017-06-24 19:36:13', '2017-06-24 19:36:13'),
(14, 'Grafite', 'img_anteprima_cover_deluxe_met_grafite.jpg', 'deluxemetallico_grafite_text.png', 0, 10, 8, 19, '2017-06-24 19:37:04', '2017-06-24 19:37:04'),
(15, 'Quarzo', 'img_anteprima_cover_deluxe_met_quarzo.jpg', 'deluxemetallico_quarzo_text.png', 0, 10, 8, 19, '2017-06-24 19:37:40', '2017-06-24 19:37:40');

-- --------------------------------------------------------

--
-- Struttura della tabella `cover__categories`
--

CREATE TABLE IF NOT EXISTS `cover__categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(10000) COLLATE utf8_unicode_ci NOT NULL,
  `associated-country` int(11) NOT NULL,
  `associated-language` int(11) NOT NULL,
  `cost` decimal(5,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cover__categories_name_unique` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Dump dei dati per la tabella `cover__categories`
--

INSERT INTO `cover__categories` (`id`, `name`, `description`, `associated-country`, `associated-language`, `cost`, `created_at`, `updated_at`) VALUES
(8, 'Le Rigide', '<p style="text-align: center; "><b>LA RIGIDA</b> è composta da una resistente copertina in cartone. È possibile scegliere fra numerose combinazioni di rivestimenti e colori</p><p style="text-align: center; ">È estremamante rigida e duratura nel tempo ed è adatta anche a contenere un numero elevato di fogli.</p>', 1, 1, '10.00', '2017-06-24 18:55:45', '2017-07-04 17:28:08'),
(9, 'Le Morbide', '<p>descrizione morbide<br></p>', 1, 1, '10.00', '2017-06-24 18:55:57', '2017-07-04 18:53:10'),
(10, 'Le Brossurate', '<p>descrizione brossurate, che sono tanto belle!<br></p>', 1, 1, '10.00', '2017-06-24 18:56:07', '2017-07-08 06:16:45'),
(11, 'Le Termiche', '<p>descrizione termiche<br></p>', 1, 1, '10.00', '2017-06-24 18:56:22', '2017-07-04 18:53:36');

-- --------------------------------------------------------

--
-- Struttura della tabella `cover__types`
--

CREATE TABLE IF NOT EXISTS `cover__types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `associated-category` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=20 ;

--
-- Dump dei dati per la tabella `cover__types`
--

INSERT INTO `cover__types` (`id`, `name`, `associated-category`, `created_at`, `updated_at`) VALUES
(16, 'Similpelle Lucido', 8, '2017-06-24 18:57:04', '2017-06-24 18:57:04'),
(17, 'Deluxe - Effetto telato', 8, '2017-06-24 18:57:24', '2017-06-24 18:58:09'),
(18, 'Deluxe - Effetto satinato', 8, '2017-06-24 18:57:40', '2017-06-24 18:57:40'),
(19, 'Deluxe - Metallico', 8, '2017-06-24 18:57:57', '2017-06-24 18:57:57');

-- --------------------------------------------------------

--
-- Struttura della tabella `languages`
--

CREATE TABLE IF NOT EXISTS `languages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `countryID` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `visible` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dump dei dati per la tabella `languages`
--

INSERT INTO `languages` (`id`, `countryID`, `name`, `visible`, `created_at`, `updated_at`) VALUES
(1, 1, 'it', 0, '2017-06-08 19:22:50', '2017-06-08 19:22:50');

-- --------------------------------------------------------

--
-- Struttura della tabella `meta_tags`
--

CREATE TABLE IF NOT EXISTS `meta_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permalink` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Dump dei dati per la tabella `meta_tags`
--

INSERT INTO `meta_tags` (`id`, `permalink`, `name`, `body`, `created_at`, `updated_at`) VALUES
(14, 'it/it/colorizer', 'tag@#0', 'content', '2017-07-04 10:12:38', '2017-07-04 10:12:38');

-- --------------------------------------------------------

--
-- Struttura della tabella `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2017_02_25_140056_create_pages_table', 1),
('2017_02_26_082333_create_countries_table', 1),
('2017_02_26_082445_create_languages_table', 1),
('2017_02_26_082501_create_components_table', 1),
('2017_03_20_112102_create_meta_tags_table', 1),
('2017_04_23_160057_create_cover__categories_table', 1),
('2017_04_23_161259_create_cover__types_table', 1),
('2017_04_23_161623_create_covers_table', 1),
('2017_04_23_162136_create_text__colors_table', 1),
('2017_02_25_140056_create_pages_table', 1),
('2017_02_26_082333_create_countries_table', 1),
('2017_02_26_082445_create_languages_table', 1),
('2017_02_26_082501_create_components_table', 1),
('2017_03_20_112102_create_meta_tags_table', 1),
('2017_04_23_160057_create_cover__categories_table', 1),
('2017_04_23_161259_create_cover__types_table', 1),
('2017_04_23_161623_create_covers_table', 1),
('2017_04_23_162136_create_text__colors_table', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country` int(11) NOT NULL,
  `language` int(11) NOT NULL,
  `permalink` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `visible` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dump dei dati per la tabella `pages`
--

INSERT INTO `pages` (`id`, `country`, `language`, `permalink`, `visible`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'it/it/colorizer', 0, '2017-06-08 19:53:02', '2017-07-04 10:12:38');

-- --------------------------------------------------------

--
-- Struttura della tabella `text__colors`
--

CREATE TABLE IF NOT EXISTS `text__colors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `background-image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `preview-image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rgb` varchar(9) COLLATE utf8_unicode_ci NOT NULL,
  `associated-category` int(11) NOT NULL,
  `associated-type` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=22 ;

--
-- Dump dei dati per la tabella `text__colors`
--

INSERT INTO `text__colors` (`id`, `name`, `background-image`, `preview-image`, `rgb`, `associated-category`, `associated-type`, `created_at`, `updated_at`) VALUES
(2, 'Argento metallizzato', 'testo_anteprima_argento_metallizzato.png', 'argento_metallizzato.png', '', 8, 16, '2017-06-24 19:39:01', '2017-06-24 19:39:01'),
(3, 'Oro Metallizato', 'testo_anteprima_oro_metallizzato.png', 'oro_metallizzato.png', '', 8, 16, '2017-06-24 19:39:31', '2017-06-24 19:39:31'),
(4, 'Blu metallizato', 'testo_anteprima_blu_metallizzato.png', 'blu_metallizzato.png', '', 8, 16, '2017-06-24 19:40:03', '2017-06-24 19:40:03'),
(5, 'Nero opaco', 'testo_anteprima_nero_opaco.png', 'nero_opaco.png', '', 8, 16, '2017-06-24 19:40:47', '2017-06-24 19:40:47'),
(6, 'Rosso opaco', 'testo_anteprima_rosso_metallizzato.png', 'rosso_metallizzato.png', '', 8, 16, '2017-06-24 19:41:25', '2017-06-24 19:41:25'),
(7, 'Argento metallizzato 2', 'testo_anteprima_argento_metallizzato - Copia.png', 'argento_metallizzato - Copia.png', '', 8, 17, '2017-06-24 19:43:05', '2017-06-24 19:43:05'),
(8, 'Oro metallizato 2', 'testo_anteprima_oro_metallizzato - Copia.png', 'oro_metallizzato - Copia.png', '', 8, 17, '2017-06-24 19:43:43', '2017-06-24 19:43:43'),
(9, 'Blu metallizato 2', 'testo_anteprima_blu_metallizzato - Copia.png', 'blu_metallizzato - Copia.png', '', 8, 17, '2017-06-24 19:44:14', '2017-06-24 19:44:14'),
(10, 'Nero opaco 2', 'testo_anteprima_nero_opaco - Copia.png', 'nero_opaco - Copia.png', '', 8, 17, '2017-06-24 19:44:49', '2017-06-24 19:44:49'),
(11, 'Rosso opaco', 'testo_anteprima_rosso_metallizzato - Copia.png', 'rosso_metallizzato - Copia.png', '', 8, 17, '2017-06-24 19:45:17', '2017-06-24 19:45:17'),
(12, 'Argento metallizzato', 'testo_anteprima_argento_metallizzato - Copia (3).png', 'argento_metallizzato - Copia (3).png', '', 8, 18, '2017-06-24 19:46:13', '2017-06-24 19:46:13'),
(13, 'Oro metallizzato', 'testo_anteprima_oro_metallizzato - Copia (3).png', 'oro_metallizzato - Copia (3).png', '', 8, 18, '2017-06-24 19:46:47', '2017-06-24 19:46:47'),
(14, 'Blu metallizzato', 'testo_anteprima_blu_metallizzato - Copia (3).png', 'blu_metallizzato - Copia (3).png', '', 8, 18, '2017-06-24 19:47:18', '2017-06-24 19:47:18'),
(15, 'Nero opaco', 'testo_anteprima_nero_opaco - Copia (3).png', 'nero_opaco - Copia (3).png', '', 8, 18, '2017-06-24 19:47:45', '2017-06-24 19:47:45'),
(16, 'Rosso opaco', 'testo_anteprima_rosso_metallizzato - Copia (3).png', 'rosso_metallizzato - Copia (3).png', '', 8, 18, '2017-06-24 19:48:19', '2017-06-24 19:48:19'),
(17, 'Argento metallizzato', 'testo_anteprima_argento_metallizzato - Copia (2).png', 'argento_metallizzato - Copia (2).png', '', 8, 19, '2017-06-24 19:48:49', '2017-06-24 19:48:49'),
(18, 'Oro metallizzato', 'testo_anteprima_oro_metallizzato - Copia (2).png', 'oro_metallizzato - Copia (2).png', '', 8, 19, '2017-06-24 19:49:19', '2017-06-24 19:49:19'),
(19, 'Blu metallizzato', 'testo_anteprima_blu_metallizzato - Copia (2).png', 'blu_metallizzato - Copia (2).png', '', 8, 19, '2017-06-24 19:50:03', '2017-06-24 19:50:03'),
(20, 'Nero opaco', 'testo_anteprima_nero_opaco - Copia (2).png', 'nero_opaco - Copia (2).png', '', 8, 19, '2017-06-24 19:50:29', '2017-06-24 19:50:29'),
(21, 'Rosso opaco', 'testo_anteprima_rosso_metallizzato - Copia (2).png', 'rosso_metallizzato - Copia (2).png', '', 8, 19, '2017-06-24 19:51:00', '2017-06-24 19:51:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
