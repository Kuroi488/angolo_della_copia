
    bookie.service('ColorizerService', function( $http ) {
        
        this.callService = function( country, language ){
            return $http.get("/ajax/returnCoversData?country=" + country + "&language=" + language ).then(function (response) {
                console.log( response.data );
                return response.data;
           }).catch( function( error ){
                    console.error( error );
                } );
        };
        
        this.getLanguages = function( country, language ){
            return $http.get( '../../assets/js/vendor/angular/langs/' + country + '-' + language + '-configurator.json' ).then( function( response ){
                return response.data;
            } ).catch( function( error ){
                    console.error( error );
                } );
        };
        
        this.getPapersWeights = function( country, language ){
            return $http.get("/ajax/returnPapersWeights?country=" + country + "&language=" + language ).then(function (response) {
                return response.data;
           }).catch( function( error ){
                    console.error( error );
                } );
        };
        
        
    });