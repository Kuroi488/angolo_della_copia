bookie.directive( 'chooseCover', function(){
  return {
    templateUrl: '../../assets/js/vendor/angular/directives/book-bindings/chooseCover.html'
  }
} );

bookie.directive( 'compileForm', function(){
  return {
    templateUrl: '../../assets/js/vendor/angular/directives/book-bindings/form.html'
  }
} );

bookie.directive( 'firstStep', function(){
  return {
    templateUrl: '../../assets/js/vendor/angular/directives/book-bindings/firstStep.html'
  }
} );

bookie.directive( 'secondStep', function(){
  return {
    templateUrl: '../../assets/js/vendor/angular/directives/book-bindings/secondStep.html'
  }
} );

bookie.directive( 'thirdStep', function(){
  return {
    templateUrl: '../../assets/js/vendor/angular/directives/book-bindings/thirdStep.html',
    link: function( $scope, elem, attrs ) {
        $scope.showPDF( 1 );
    }
  }
} );

bookie.directive( 'fourthStep', function(){
  return {
    templateUrl: '../../assets/js/vendor/angular/directives/book-bindings/fourthStep.html'
  }
} );

bookie.directive( 'fifthStep', function(){
  return {
    templateUrl: '../../assets/js/vendor/angular/directives/book-bindings/fifthStep.html'
  }
} );

bookie.filter('toHTML', function() {
    
    function getAllElementsWithAttribute(attribute) {
      
      var matchingElements = [];
      var allElements = document.getElementsByTagName('*');
      
      for (var i = 0, n = allElements.length; i < n; i++) {
        if ( allElements[i].getAttribute(attribute) !== null ) {
              // Element exists with attribute. Add to array.
              matchingElements.push(allElements[i]);
        }
      }
      
      return matchingElements[0];
    }

    return function( input ) {
      if ( input ) {
          var el = getAllElementsWithAttribute( 'data-toHTML' );
          el.innerHTML = input;
      }
    };
  
});