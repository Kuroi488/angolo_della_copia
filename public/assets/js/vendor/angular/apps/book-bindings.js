
bookie = angular.module( 'bookie', ['uiCropper'] );
    


  bookie.controller( 'baseController',
                     ['$scope', 'ColorizerService', '$http', '$rootScope', '$compile',
                       function( $scope, ColorizerService, $http, $rootScope, $compile ){      
                         
                         
                         
    // variables
    $scope.hideConfigurator = ( $( window ).width() > 600 ) ? true : false;
    $scope.direction  = window.location.toString().split( '/' );
      $scope.country  = $scope.direction[ 3 ];
      $scope.language = $scope.direction[ 4 ];
      
    $scope.pageNumber     = 5;
    $scope.cdState        = 'toDecide';
    $scope.myImage        = '';
    $scope.myCroppedImage = '';
    $scope.pagination     = '';
    $scope.makeCD         = true;
    
    
    $scope.copiesQuantity = 1;
    $scope.coloredPages   = 0;
    $scope.greyPages      = 0;
    $scope.pdf            = '';
    $scope.canvasOriginal = {};
    
    $scope.PDFerror        = false;
    $scope.frontError      = false;
    $scope.paginationError = false;
    $scope.numPagesError   = false;
    
    // variables end
                  
    // services
    
    
    ColorizerService.callService( $scope.country, $scope.language ).then( function( data ){
        
          $scope.categories = data;
          $scope.category   = $scope.categories[0];
          $scope.types      = $scope.category.types;
          $scope.coverType  = $scope.types[0];
          $scope.coverColor = $scope.coverType.covers[0];
          $scope.textColor  = $scope.coverType.colors[0];
        
          setTimeout( function(){
            $( 'body' ).find( '.coverDiv' ).eq( 0 ).addClass( 'active' );
            $( 'body' ).find( '.colorDiv' ).eq( 0 ).addClass( 'active' );
          }, 200 );
        
      } );
      
    ColorizerService.getLanguages( $scope.country, $scope.language ).then( function( response ){
        $scope.configuratorLangs = response;
        
        $scope.colorCost     = parseFloat( $scope.configuratorLangs.color_cost ).toFixed(2);
        $scope.greyScaleCost = parseFloat( $scope.configuratorLangs.grey_scale_cost ).toFixed(2);
        $scope.cdCost        = parseFloat( $scope.configuratorLangs.cdCost ).toFixed(2);
        
    } );
    
    ColorizerService.getPapersWeights( $scope.country, $scope.language ).then( function( response ){
        $scope.papersWeights = response;
    } );
    
    // services end
    
    // common functions
    
    function setColorConfig(){
      
      var data = { category: $scope.category, type: $scope.coverType, coverColor: $scope.coverColor, textColor: $scope.textColor }
      $http.post( '/set_colorizer_config', data ).then(function (response) {
              // console.log( response );
           })
    };
    
    function setOrderConfig( data ){
      $http.post( '/set_order_config', data ).then(function (response) {
               $scope.resp = response.data;
           });
    };
    
    function chooseInput( event ){
      $( '.customInputWrapper' ).find( 'input' ).prop( 'checked', false );
      $( event.target ).prop( 'checked', true );
    }
    
    
    $( window ).on( 'resize', function(){
      var width = $( window ).width();
        if ( width < 600 ){
                $scope.$apply( function(){
                  $scope.hideConfigurator = false;
                });
        } else {
            $scope.$apply( function(){
                  $scope.hideConfigurator = true;
            });
        }
    
    } );
    
    $scope.changeCopiesQuantity = function( operand ){
      
      if ( operand === 'LESS' && $scope.copiesQuantity === 1 ) { return; }
      
      if ( operand === 'MORE' ) {
          $scope.copiesQuantity++;
      } else {
          $scope.copiesQuantity--;
      }
      
    };
    
    $scope.replaceString = function( string ) {
      var replacedString = string.join( '\n' );
      return replacedString;
    };
    
    function reduceMemory(){
      $scope.canvasOriginal = {};
      $scope.myImage='';
      $scope.myCroppedImage='';
    }
    
    $scope.switchColor = function( $event ){
      
      var element   = $( $event.target );
      var canvasObj = element.siblings('canvas');
      var canvasKey = canvasObj.attr( 'id' );
      var canvas    = canvasObj.get()[0];
      var ctx       = canvas.getContext("2d");
      var width     = canvas.width;
      var height    = canvas.height;
      var imageData = ctx.getImageData( 0, 0, width, height );
      
      if ( element.attr( 'data-color' ) === 'grey' &&
           !$scope.canvasOriginal[ canvasKey ] ) {
             
             // no color, avoid all actions
             return;
             
      } else if ( element.attr( 'data-color' ) === 'grey' &&
                  $scope.canvasOriginal[ canvasKey ] ) {
                    
                    // reset original color
                    imageData.data.set( $scope.canvasOriginal[ canvasKey ] );
                    ctx.putImageData( imageData, 0, 0 );
                    element.attr( 'data-color', 'color' );
                    return canvas;  
                    
      } else {
      
        $scope.canvasOriginal[ canvasKey ] = new Uint8ClampedArray(imageData.data);
        var data = imageData.data;

        for ( var i = 0; i < data.length; i += 4 ) {
          var luma = ( data[ i ] + data[ i + 1 ] + data[ i + 2 ] ) / 3;
          imageData.data[ i ]     = luma;
          imageData.data[ i + 1 ] = luma;
          imageData.data[ i + 2 ] = luma;
        }
    
        ctx.putImageData(imageData, 0, 0);
        element.attr( 'data-color', 'grey' );
        return canvas;
        
      }
      
    };
    
    function preparePageCanvas( idNumber ) {
      
      var pageBLockHTML = '<div class="pageWrapper">' + 
                            '<canvas id="canvas_' + idNumber + '" data-original="" ></canvas>' + 
                            '<label class="colorSwitcher" ng-click="switchColor( $event )"></label>' +
                            '<div class="pageLabel"></div>' +
                            '<div class="pageTransform"></div>' +
                          '</div>';
                          
      var pageBlock = $compile( pageBLockHTML )( $scope );
      
      angular.element(document.getElementById('canvasWrapper')).append(pageBlock);
      
      var canvas = document.getElementById( 'canvas_' + idNumber );
      return canvas;
      
    }
    
    $scope.colorCheck = function( number ){
      
          var currentID = '#canvas_' + number;

          var colorThief      = new ColorThief();
          var possiblePalette = colorThief.getColor( $( currentID ).get()[0] );
          var r               = possiblePalette[0];
          var g               = possiblePalette[1];
          var b               = possiblePalette[2];
          
          if( ( r + g + b ) < 460 ){
            
            $( currentID ).siblings( 'label' ).attr( 'data-color', 'grey' );
            $( currentID ).siblings( '.pageLabel' ).text( $scope.configuratorLangs.page + ' ' + number );
            $( currentID ).siblings( '.pageTransform' ).text( $scope.configuratorLangs.convert_to_COLOR );
            $scope.$apply( function(){
              $scope.greyPages++;
            });
          
          } else {
            
            $( currentID ).siblings( 'label' ).attr( 'data-color', 'color' );
            $( currentID ).siblings( '.pageLabel' ).text( $scope.configuratorLangs.page + ' ' + number );
            $( currentID ).siblings( '.pageTransform' ).text( $scope.configuratorLangs.convert_to_BW );
            $scope.$apply( function(){
              $scope.coloredPages++;
            });
            
          }
          
          $scope.showPDF( number + 1 );
          
  };
  
  $scope.showPDF = function( pageNumber ) {
    
    if ( pageNumber > $scope.pdf.numPages ) {
      $( '.nextStep' ).attr( 'disabled', false );
      return;
    }
    
    $scope.pdf.getPage( pageNumber ).then( function( page ){
            
            var scale    = 0.25;
            var viewport = page.getViewport(scale);
            
            var canvas        = preparePageCanvas( pageNumber );
                canvas.height = viewport.height;
                canvas.width  = viewport.width;
            var context       = canvas.getContext('2d');
                
            var task = page.render({canvasContext: context, viewport: viewport})
            task.promise.then(function(){
              $scope.colorCheck( pageNumber ); 
            });
            
          });
  }
        // common functions end
    
  
    // colorizer
    
    $scope.getCategory = function( category ){
      $scope.category   = category;
      $scope.types      = category.types;
      $scope.coverType  = category.types[0];
      $scope.coverColor = category.types[0].covers[0];
      $scope.textColor  = category.types[0].colors[0];
      
      setColorConfig();
      
    };
    
    $scope.getTypeAndCover = function( type, cover, event ){
      
      $scope.coverType = type;
      $scope.coverColor = cover;
      
      
      $scope.textColor = type.colors[0];
        
        if( $( event.target ).is( 'div' ) === true ){
            var triggered = $( event.target );
        } else {
            var triggered = $( event.target ).closest( 'div' );
        }
        
        $( '.coverDiv' ).removeClass( 'active' );
        triggered.addClass( 'active' );
        
        $( '.colorDiv' ).removeClass( 'active' );
        setTimeout( function(){
            $( 'body' ).find( '.colorDiv' ).eq( 0 ).addClass( 'active' );
        }, 200 );
        
      setColorConfig();
        
    };
    
    $scope.getTextColor = function( textColor, event ){
      $scope.textColor = textColor;
        
        if( $( event.target ).is( 'div' ) === true ){
            var triggered = $( event.target );
        } else {
            var triggered = $( event.target ).closest( 'div' );
        }
        
        $( '.colorDiv' ).removeClass( 'active' );
        triggered.addClass( 'active' );
        
        setColorConfig();
        
    };
      
    
    // colorizer end
    
    // configurator
    
    $scope.cdOnlyWithImage = function(){ $scope.cdState = 'withImage'; }
    $scope.cdOnlyWithText = function(){ $scope.cdState = 'onlyText'; }
    
    function getRoundedCanvas(sourceCanvas) {
      var canvas = document.createElement('canvas');
      var context = canvas.getContext('2d');
      var width = sourceCanvas.width;
      var height = sourceCanvas.height;
      canvas.width = width;
      canvas.height = height;
      context.beginPath();
      context.arc(width / 2, height / 2, Math.min(width, height) / 2, 0, 2 * Math.PI);
      context.strokeStyle = 'rgba(255,255,255,1)';
      context.stroke();
      context.clip();
      context.drawImage(sourceCanvas, 0, 0, width, height);
      return canvas;
    }
    
    function manageUpload( uploadData ) {
      
      $scope.myImage        = '';
      $scope.myCroppedImage = '';
      
      var imageType = uploadData[ 'imageType' ];
      var evt       = uploadData[ 'event' ];
      var file      = evt.currentTarget.files[0];
      
      if ( imageType === 'cdImage' ) {
        
          var reader = new FileReader();
          reader.onload = function (evt) {
            $scope.$apply(function($scope){
              $scope.myImage=evt.target.result;
              $scope.myCroppedImage = evt.target.result;       
            });
          };
      
          reader.readAsDataURL(file);
          
        
      } else if ( imageType === 'frontImage' ) {
        
          var reader = new FileReader();
          reader.onload = function (evt) {
              var data = { frontImage: evt.target.result };
              setOrderConfig( data );
          };
          reader.readAsDataURL(file);
      }
      
      $( '.nextStep' ).attr( 'disabled', false );
      $( '.previousStep' ).attr( 'disabled', false );
        
        
    }
     
    
    function setNumbersColors() {
      
      $( '.configurator-step' ).removeClass( 'current ').removeClass( 'passed' );
      
      $( '.configurator-step' ).each( function(){
        
        var position = $( this ).index() + 1;
        
        if ( position < $scope.pageNumber ) {
          $( this ).addClass( 'passed' );
        } else if ( position === $scope.pageNumber ){
          $( this ).addClass( 'current' );
        }
        
      } );
      
    };
    
    $scope.checkSelection = function( scopeID, elementID ) {
      if( scopeID === null || elementID === null ) { return false; }
      return ( ( scopeID === elementID ) ? 'checked' : false );
    };
    
    function userSelection( $event, type ) {
      
      if ( $( $event.target ).is( '.valueWrapper' ) === true ) {
            var triggered = $( $event.target );
      } else {
            var triggered = $( $event.target ).closest( '.valueWrapper' );
      }
      
      if ( type === 'input' ){
        
        triggered.siblings().find( 'input' ).removeAttr( 'checked' );
        triggered.find( 'input' ).prop( 'checked', 'checked' );
        
      } else {
        
        triggered.siblings().find( 'img' ).removeClass( 'active' );
        triggered.find( 'img' ).addClass( 'active' );
        
      }
      
    };
    
    $scope.getCategoryAndType = function( category, type, $event ){
      $scope.category = category;
      $scope.coverType = type;
      $scope.coverColor = type.covers[0];
      $scope.textColor = type.colors[0];
      
      
      userSelection( $event, 'input' );
      
      setColorConfig();
      
      var data = { category:   { category_name: category.name, category_id: category.id, category_cost: category.cost },
                   cover_type: { cover_type_name: type.name, cover_type_id: type.id }
                  };
                  
      setOrderConfig( data );
    };
    
    $scope.saveWeight_inOrderConfig = function ( weight, $event ) {
      
      userSelection( $event, 'input' );
      $scope.storedWeight = weight;
      $( '.order_temp' ).show();
      var data = { weight_name: weight.name, weight_id: weight.id, weight_weight: weight.weight };
      setOrderConfig( { weight: data } );
      
      $scope.weightError = false;
      
    };
    
    $scope.saveTextColor_inOrderConfig = function ( color, $event ) {
      userSelection( $event, 'img' );
      $scope.textColor = color;
      var data = { color_name: color.name, color_id: color.id };
      setOrderConfig( { text: data } );
    };
    
    $scope.saveCoverColor_inOrderConfig = function ( color, $event ) {
      userSelection( $event, 'img' );
      $scope.coverColor = color;
      var data = { color_name: color.name };
      setOrderConfig( { cover: data } );
    };
    
    $scope.setPagination = function( pagination, event ) {
      if ( event ){
        chooseInput( event );
      }
      setOrderConfig( { 'pagination': pagination } );
      $scope.pagination = pagination;
    };
    
    $scope.setDelivery = function( delivery, event ) {
      chooseInput( event );
      setOrderConfig( { 'delivery': delivery } );
    };
    
    $scope.saveCDCoverTexts_inOrderConfig = function() {
      
      $scope.pageNumber++;
      var data = { 'CD_cover': {} };
      $scope.cdCost = parseFloat( $scope.configuratorLangs.cdCost ).toFixed( 2 );
      $( '.textRound input, .textRound textarea').each( function(){
        var attributeName = $( this ).attr( 'class' );
        var value = $( this ).val();
        data[ 'CD_cover' ][ attributeName ] = value;
      } );
      setOrderConfig( data );
      $scope.goToNextPage( null );
    }
    
    $scope.deleteCDcover = function() {
      $scope.cdState = 'toDecide';
      $scope.makeCD  = false;
      $scope.cdCost  = 0;
      setOrderConfig( { CD_cover: '' } );
      console.log( 'avoid cover' );
      setTimeout( function(){
        $( '.previousStep, .nextStep' ).attr( 'disabled', false );
      }, 500 );
    };
    
    $scope.saveCDCoverImages_inOrderConfig = function() {
      $scope.cdCost = parseFloat( $scope.configuratorLangs.cdCost ).toFixed( 2 );
      $scope.goToNextPage( null );
    }
    
    $scope.uploadImage = function( data ) {
      
      $( '.extesionError' ).hide();
      
      var evt = data['event'];
      var $input = evt.target;
      var limits = $( $input ).attr( 'accept' ).replace( / /g, '' ).split( ',' );
      var fileExtension = $input.files[ 0 ].type;
      
      
      if ( limits.indexOf( fileExtension ) !== -1 ) {
        
        manageUpload( data );
        
      } else {
        
        $( $input ).val( '' );
        $( '.extesionError' ).show();
        
      }
    
    };
    
    
    // configurator end
    
    // upload pdf
    
      $scope.uploadPDF = function( event  ){
        $scope.pdf = '';
        PDFJS.disableWorker = true;
          var file = event.target.files[ 0 ];
          var fileReader = new FileReader();
          fileReader.onload = function( ev ) {
            PDFJS.getDocument( fileReader.result )
                 .then( function getPdfHelloWorld( pdf ) {
                  
                    if ( pdf.numPages > 339 ) {
                      $scope.$apply( function(){
                        $scope.numPagesError = true;
                      });  
                    } else {
                      $scope.$apply( function(){
                        $scope.numPagesError = false;
                      });
                      $scope.pdf = pdf;
                    }

                $( '.nextStep' ).attr( 'disabled', false );
              }, function( error ){
                console.log( error );
              });
            };
            fileReader.readAsArrayBuffer( file );
            $scope.PDFerror = false;
      };
      
    
    
    // upload PDF
    
    // pages navigation
    
    function checkPageNavigation(){
      
      $( '.stepsButtons' ).find( 'button' ).attr( 'disabled', false );
      
      switch( $scope.pageNumber ) {
        case 1:
          $( '.previousStep' ).attr( 'disabled', 'disabled');
        break;
        case 2:
          
        break;
        case 3:
          var data = { categoryID:     $scope.category.id,
                       categoryName:   $scope.category.name,
                       typeID:         $scope.coverType.id,
                       typeName:       $scope.coverType.name,
                       coverColorID:   $scope.coverColor.id,
                       coverColorName: $scope.coverColor.name,
                       textColorID:    $scope.textColor.id,
                       textColorName:  $scope.textColor.name
            
                      }
          setOrderConfig( data );
          $( '.nextStep' ).attr( 'disabled', 'disabled ')
                          .text( $scope.configuratorLangs.proceed );
        break;
        case 4:
          $( '.nextStep' ).text( $scope.configuratorLangs.proceed_without_cd );
          reduceMemory();
        break;
        case 5:
          if ( $scope.cdState === 'toDecide' ) {
            $scope.makeCD = false;
          }
          
          setTimeout( function(){
            $( '.nextStep' ).attr( 'disabled', false )
                            .text( $scope.configuratorLangs.proceed );
          }, 500 );
          $( '.previousStep' ).css( 'display', 'none' );
        break;
        case 6:
          $( '.nextStep' ).css( 'display', 'none' );
          $( '.previousStep' ).css( 'display', 'block' );
        break;
      }
      
    };
    
    $scope.goToNextPage = function( $event ){
      
      if ( $scope.pageNumber === 1 ) {
      
        if ( $scope.pdf === '' ) {
              $scope.PDFerror = true;
              return;
        } else if ( $( '.only_front' ).is( ':checked' ) === false &&
                    $( '.front_and_back' ).is( ':checked' ) === false ) {
                        $scope.paginationError = true;
                        return;
        }
        
      }
      
      
      if ( $scope.pageNumber === 2 && $scope.storedWeight === undefined ) {
        $scope.weightError = true;
        return;
      }
      
      if( $scope.pageNumber === 6 ) { return; }
      $scope.pageNumber = $scope.pageNumber + 1;
      checkPageNavigation();
      setNumbersColors();
    };
      
    $scope.goToPreviousPage = function( $event ){
      if( $scope.pageNumber === 1 ) { return; } 
      $scope.pageNumber = $scope.pageNumber - 1;
      checkPageNavigation();
      setNumbersColors();
    };
      
    // pages navigation end

  }]);
