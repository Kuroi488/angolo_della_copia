var gulp = require('gulp');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var strip = require('gulp-strip-comments');
var stripDebug = require('gulp-strip-debug');
var concat = require('gulp-concat');
var concatCss = require('gulp-concat-css');
var autoprefix = require('gulp-autoprefixer');
var imageop = require('gulp-image-optimization');
var babel = require('gulp-babel');


var paths = {
    js: [
        'resources/assets/js/numeric.js',
        'resources/assets/js/base.js',
        'resources/assets/js/data-entry.js',
    ],
    sass: [
          'resources/assets/sass/app.scss',
          'resources/assets/sass/office.scss',
          'resources/assets/sass/data-entry.scss',
    ]
};

var front_end_paths = {
  js: [
      'resources/components/how-it-works/how-it-works.js',
  ],
  sass: {
        'common': 'resources/components/main/sass/main.scss',
        'others': [
          'resources/components/*/*.scss'
        ]

        }
};

/* uglify */
gulp.task('scripts', function() {
    gulp.src(paths.js)
        .pipe(concat('main.js'))
        // .pipe(strip())
        //.pipe(stripDebug())
        // .pipe(uglify())
        .pipe(gulp.dest('public/assets/js/'));
});

gulp.task('frontend-scripts', function(){
    gulp.src(front_end_paths.js)
        // .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['es2015']
        }))
        // .pipe(concat('main.js'))
        // .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('public/assets/frontend/js/'));
});

// gulp.task('frontend-scripts', function() {
//   gulp.src(front_end_paths.js)
//         // .pipe(concat('main.js'))
//         // .pipe(strip())
//         //.pipe(stripDebug())
//         // .pipe(uglify())
//         .pipe(gulp.dest('public/assets/frontend/js/'));
// });

/* sass */
gulp.task('sass', function () {
  gulp.src(paths.sass)
    .pipe(sass().on('error', sass.logError))
    .pipe(concatCss('main.css'))
    .pipe(gulp.dest('public/assets/css/'));

});
// gulp.task('frontend-sass', function () {
//   gulp.src(front_end_paths.sass.others)
//     .pipe(sass().on('error', sass.logError))
//     .pipe(gulp.dest('public/assets/frontend/css/'));
//
// });
gulp.task('frontend-main-sass', function () {
  gulp.src(front_end_paths.sass.common)
    .pipe(sass().on('error', sass.logError))
    .pipe(concatCss('main.css'))
    .pipe(gulp.dest('public/assets/frontend/css/'));

});

/* images */
gulp.task('images', function(cb) {
    gulp.src([
      'resources/assets/img/*.png',
      'resources/assets/img/*.jpg',
      'resources/assets/img/*.gif',
      'resources/assets/img/*.jpeg',
      'resources/assets/img/covers/*.png',
      'resources/assets/img/covers/*.jpg',
      'resources/assets/img/covers/*.gif',
      'resources/assets/img/covers/*.jpeg',
    ]).pipe(imageop({
        optimizationLevel: 5,
        progressive: true,
        interlaced: true
    })).pipe(gulp.dest('public/assets/images/')).on('end', cb).on('error', cb);
});

gulp.task('frontend-images', function(cb) {
    gulp.src([
      'resources/components/main/images/*.png',
      'resources/components/main/images/*.jpg',
      'resources/components/main/images/*.gif',
      'resources/components/main/images/*.jpeg'
    ]).pipe(imageop({
        optimizationLevel: 5,
        progressive: true,
        interlaced: true
    })).pipe(gulp.dest('public/assets/frontend/images/')).on('end', cb).on('error', cb);
});
/* watcher */
gulp.task('watch', ['scripts',
                    'sass',
                    'images',
                    // front office
                    'frontend-scripts',
                    // 'frontend-sass',
                    'frontend-main-sass',
                    'frontend-images'
                  ],
    function() {
    gulp.watch('resources/assets/js/**/*.js', ['scripts']);
    gulp.watch('resources/assets/sass/*.scss', ['sass']);
    gulp.watch('resources/assets/img/*.png', ['images']);
    gulp.watch('resources/assets/img/*.jpg', ['images']);
    gulp.watch('resources/assets/img/*.gif', ['images']);
    gulp.watch('resources/assets/img/*.jpeg', ['images']);
    gulp.watch('resources/assets/img/covers/*', ['images']);
    // front office
    gulp.watch('resources/components/**/*.js', ['frontend-scripts']);
    gulp.watch([ 'resources/components/main/sass/*.scss', 'resources/components/**/*.scss' ], ['frontend-main-sass']);
    gulp.watch('resources/components/main/images/*.png', ['frontend-images']);
    gulp.watch('resources/components/main/images/*.jpg', ['frontend-images']);
    gulp.watch('resources/components/main/images/*.gif', ['frontend-images']);
    gulp.watch('resources/components/main/images/*.jpeg', ['frontend-images']);
});


gulp.task('default', ['sass',
                      'scripts',
                      'images',
                      // front office
                      'frontend-scripts',
                      // 'frontend-sass',
                      'frontend-main-sass',
                      'frontend-images',
                      'watch'
                    ] );
