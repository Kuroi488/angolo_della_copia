
<?php use App\libraries\DataEntry; ?>

  @if( count( $language > 0 ) )
    <?php include resource_path( 'lang/' . $language['country'] . '/' . $language['language'] . '/' . 'footer.php' ); ?>
  @endif

  <div class="" style="clear: both; width: 100%"> </div>

  <footer class="container-fluid footer_wrapper">
    <div class="col-lg-10 col-lg-offset-1">
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 list-wrapper footer-column">
        <ul> <?php echo DataEntry::bailString( $footer, 'who', 'text' ); ?>
          <li>
            <a class="" href="<?php echo $language['country'] . '/' . $language['language'] . '/' . DataEntry::bailString( $footer, 'FAQS', 'link' ); ?>" >
              <?php echo DataEntry::bailString( $footer, 'FAQS', 'text' ); ?>
            </a>
          </li>
        </ul>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 list-wrapper footer-column">
        <ul> <?php echo DataEntry::bailString( $footer, 'privacy', 'text' ); ?>
          <li>
            <a class="" href="<?php echo $language['country'] . '/' . $language['language'] . '/' . DataEntry::bailString( $footer, 'our-shops', 'link' ); ?>" >
              <?php echo DataEntry::bailString( $footer, 'our-shops', 'text' ); ?>
            </a>
          </li>
        </ul>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 social-wrapper footer-column">
        <div class="col-lg-2 col-lg-offset-3 col-md-2 col-md-offset-3 col-sm-2 col-xs-2 col-xs-offset-3">
          <a class="" href="<?php echo $language['country'] . '/' . $language['language'] . '/' . DataEntry::bailString( $footer, 'social_twitter', 'link' ); ?>" >
            <div class="rounded facebook">
              <?php echo DataEntry::bailString( $footer, 'social_twitter', 'text' ); ?>
            </div>
          </a>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
          <a class="" href="<?php echo $language['country'] . '/' . $language['language'] . '/' . DataEntry::bailString( $footer, 'social_facebook', 'link' ); ?>" >
            <div class="rounded facebook">
              <?php echo DataEntry::bailString( $footer, 'social_facebook', 'text' ); ?>
            </div>
          </a>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
          <a class="" href="<?php echo $language['country'] . '/' . $language['language'] . '/' . DataEntry::bailString( $footer, 'social_g-plus', 'link' ); ?>" >
            <div class="rounded facebook">
              <?php echo DataEntry::bailString( $footer, 'social_g-plus', 'text' ); ?>
            </div>
          </a>
        </div>
        <br class="hidden-lg hidden-md"/><br class="hidden-lg hidden-md"/>
        <div class="col-lg-7 col-lg-offset-3 col-md-7 col-md-offset-3 col-sm-6 col-xs-12 text-center string">
          <a class="" href="<?php echo $language['country'] . '/' . $language['language'] . '/' . DataEntry::bailString( $footer, 'help', 'link' ); ?>" >
            <?php echo DataEntry::bailString( $footer, 'help', 'text' ); ?>
          </a>
        </div>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 payment-wrapper footer-column">
        <ul> CHI SIAMO
          <li>DOMANDE FREQUENTI</li>
        </ul>
      </div>
    </div>

  </footer>
