<div class="form-group">
    <div class="checkbox">
      <label for="visible">
        <?php
          $value = ( $variable->visible != 0 ) ? 0 : 1;
          $alterState = ( $variable->visible != 0 ) ? 'Nascondi' : 'Pubblica';
          echo Form::checkbox('visible', $value );
          echo $alterState;
        ?>
      </label>
    </div>
</div>
