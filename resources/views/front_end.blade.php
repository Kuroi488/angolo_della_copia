
<?php use App\libraries\DataEntry; ?>

  @if( count( $language > 0 ) )
    <?php include resource_path( 'lang/' . $language['country'] . '/' . $language['language'] . '/' . 'menu.php' ); ?>
  @endif
  
  <nav class="navbar navbar-default container-fluid menu_background">
  <div class="">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header" style="padding-bottom: 5px;">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
        <a class="navbar-brand hidden-sm hidden-md hidden-lg" href="home" style="">
          <img src="{{ asset( 'assets/frontend/images/logo-angolo-della-tesi.png' ) }}" alt="">
        </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse menu-wrapper" id="bs-example-navbar-collapse-1">
      <div class="row floating-titles">
        <div class="col-md-2 col-sm-2 col-xs-12 text-center link hidden-xs">
          <a class="navbar-brand" href="home">
            <img src="{{ asset( 'assets/frontend/images/logo-angolo-della-tesi.png' ) }}" alt="">
          </a>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-12 text-center link <?php echo DataEntry::getActiveString( $menu, 'how' ); ?>">
          <a class="navbar-brand"
             href="<?php echo DataEntry::bailString( $menu, 'how', 'link' ); ?>">
            <?php echo DataEntry::bailString( $menu, 'how', 'text' ); ?>
          </a>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-12 text-center link  <?php echo DataEntry::getActiveString( $menu, 'covers' ); ?>">
          <a class="navbar-brand"
             href="<?php echo DataEntry::bailString( $menu, 'covers', 'link' ); ?>"
          >
            <?php echo DataEntry::bailString( $menu, 'covers', 'text' ); ?>
          </a>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-12 text-center link  <?php echo DataEntry::getActiveString( $menu, 'GAQ' ); ?>">
          <a class="navbar-brand"
             href="<?php echo DataEntry::bailString( $menu, 'GAQ', 'link' ); ?>">
            <?php echo DataEntry::bailString( $menu, 'GAQ', 'text' ); ?>
          </a>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-12 text-center link  <?php echo DataEntry::getActiveString( $menu, 'exped' ); ?>">
          <a class="navbar-brand"
             href="<?php echo DataEntry::bailString( $menu, 'exped', 'link' ); ?>">
            <?php echo DataEntry::bailString( $menu, 'exped', 'text' ); ?>
          </a>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-12 text-center link  <?php echo DataEntry::getActiveString( $menu, 'help' ); ?>">
          <a class="navbar-brand"
             href="<?php echo DataEntry::bailString( $menu, 'help', 'link' ); ?>">
            <?php echo DataEntry::bailString( $menu, 'help', 'text' ); ?>
          </a>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-12 text-center link  <?php echo DataEntry::getActiveString( $menu, 'print' ); ?>">
          <a class="navbar-brand"
             href="<?php echo DataEntry::bailString( $menu, 'print', 'link' ); ?>">
            <?php echo DataEntry::bailString( $menu, 'print', 'text' ); ?>
          </a>
        </div>
      </div>

    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
