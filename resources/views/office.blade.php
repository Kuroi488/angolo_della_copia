<!DOCTYPE html>

<?php
use App\Http\Controllers\CoversController;
use App\libraries\BreadCrumbs;
?>
<html>
    <head>
        <meta charset="utf-8">
        <title>Office section</title>
        <link rel="stylesheet" type="text/css" href="{{asset( 'assets/css/bootstrap.min.css' )}}">
        <link rel="stylesheet" type="text/css" href="{{asset( 'assets/css/bootstrap-theme.min.css' )}}">
        <link rel="stylesheet" type="text/css" href="{{asset( 'assets/css/font-awesome.min.css' )}}">
        <link rel="stylesheet" type="text/css" href="{{asset( 'assets/css/jquery-ui.min.css' )}}">
        <link href="{{asset( 'assets/js/vendor/summernote/summernote.css' )}}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{asset( 'assets/css/main.css' )}}">
        <script type="text/javascript" src="{{asset( 'assets/js/jquery.js' )}}"></script>
        <script type="text/javascript" src="{{asset( 'assets/js/jquery-ui.min.js' )}}"></script>
        <script type="text/javascript" src="{{asset( 'assets/js/bootstrap.min.js' )}}"></script>
        <script type="text/javascript" src="{{asset( 'assets/js/paging.js' )}}"></script>
        <script src="{{asset( 'assets/js/vendor/summernote/summernote.js' )}}"></script>
        <script type="text/javascript" src="{{asset( 'assets/js/main.js' )}}"></script>


    </head>
    <body>

      <nav class="navbar navbar-inverse">
        <div class="container-fluid">

          <div class="navbar-header">
            <button type="button"
                    name="button"
                    class="navbar-toggle collapsed"
                    data-toggle="collapse"
                    data-target="#bs-example-navabar-collapse-1"
                    aria-expanded="false">
                <span class="sr-only"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Brand</a>

          </div>

          <div class="collapse navbar-collapse text-right" id="bs-example-navabar-collapse-1">
            <ul class="nav navbar-nav">
              <li class="dropdown">
                <a href="#"
                   class="dropdown-toggle"
                   data-toggle="dropdown"
                   role="button"
                   aria-haspopup="true"
                   aria-expanded="false">
                   Gestione ( Data-Entry e Risorse )
                   <span class="caret"></span>
                 </a>
                 <ul class="dropdown-menu">
                     <li><a href="/office">Menu</a></li>
                     <li role="separator" class="divider"></li>
                     <li><a href="/office/warehouse/covers/index">Copertine</a></li>
                     <li><a href="/office/warehouse/colors/index">Colori del Testo</a></li>
                     <li><a href="/office/warehouse/papers/index">Grammature</a></li>
                     <li role="separator" class="divider"></li>
                     <li><a href="/office/data-entry/index">Data-entry</a></li>
                   <!-- <li><a href="#">Crea Materiale</a></li> -->
                 </ul>
              </li>
              <li class="dropdown">
                <a href="#"
                   class="dropdown-toggle"
                   data-toggle="dropdown"
                   role="button"
                   aria-haspopup="true"
                   aria-expanded="false">
                   Clienti
                   <span class="caret"></span>
                 </a>
                 <ul class="dropdown-menu">
                   <li><a href="#">Elenco Clienti</a></li>
                 </ul>
              </li>
              <li class="dropdown">
                <a href="#"
                   class="dropdown-toggle"
                   data-toggle="dropdown"
                   role="button"
                   aria-haspopup="true"
                   aria-expanded="false">
                   Ordini
                   <span class="caret"></span>
                 </a>
                 <ul class="dropdown-menu">
                   <li><a href="#">Elenco Ordini</a></li>
                   <li role="separator" class="divider"></li>
                   <li><a href="#">Crea ordine</a></li>
                 </ul>
              </li>
            </ul>

          </div>

        </div>

      </nav>
      
      <div style="clear: both; height: 30px;"></div>
      
        <div class="container">
            <div class="content">
              @yield('office_body')
            </div>
        </div>
        
    </body>
</html>
