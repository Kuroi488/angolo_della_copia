



@extends( 'office' )

@section( 'office_body' )

<?php
    use App\Models\Cover_Category;
    if( !isset( $category ) ){ return view( '404' ); }
//    $category = $category[ 0 ];
    $path = Cover_Category::updatePath( $category->id );
?>

<section>

    <h2>Modifica Categoria</h2>

  <?php echo Form::open(['url' => $path, 'method' => 'POST', 'class' => 'col-md-6']);
        echo Form::token();
  ?>
    
    
  <input type="hidden" name="id" value="<?php echo $category->id; ?>" />

  <div class="form-group">
    <?php
      echo Form::label('name', 'Nome:', ['class' => 'control-label']);
      echo Form::text('name', $category->name, array_merge(['class' => 'form-control']));
    ?>
  </div>
    
   @if(count($countries)>0)
      <div class="form-group">
        <?php
          echo Form::label('country', 'Seleziona Paese:', array('class' => 'control-label'));
          echo '<select id="country" name="country" class="form-control">';
          echo '<option value="0">Selezionare Paese</option>';
          foreach( $countries as $country ){
            $selected = ( $country['id'] == $category[ 'associated-country' ] ) ? 'selected="selected"' : '';
            echo "<option value='{$country['id']}' $selected > {$country['name']} </option>";
          }
          
          echo '</select>';
        ?>
      </div>
    
      <div class="form-group">
        <label for="language" class="control-label">Seleziona lingua:</label>
            <select class="form-control" id="language" name="language">
                <option value="0">Selezionare Lingua</option>
                    <?php
                        foreach( $languages as $language ){
                          $selected = ( $language['id'] == $category[ 'associated-language' ] ) ? 'selected="selected"' : '';
                            echo "<option value='{$language['id']}' $selected > {$language['name']} </option>";
                        }
            //              foreach( $languages as $language ){
            //                if( $language['countryID'] == $page->country ){
            //                  $selected = ( $language['id'] == $page->language ) ? 'selected="selected"' : '';
            //                  echo '<option value="' . $language['id'] . '">' . $language['name'] . '</option>';
            //                }
            //              }
                    ?>
            </select>
       </div>
   @endif
    
    <div class="form-group">
        <?php
          echo Form::label('description', 'Descrizione:', ['class' => 'control-label']);
          echo Form::textarea('description', $category->description, array_merge(['class' => 'form-control summerText']));
        ?>
    </div>
    <div class="form-group">
      <?php
        echo Form::label('cost', 'Costo:', ['class' => 'control-label']);
        echo Form::text('cost', $category->cost, array_merge(['class' => 'form-control']));
      ?>
    </div>
    
   <button type="submit" class="btn btn-default">Aggiorna</button>

    <?php echo Form::close(); ?>
  
</section>

  <div class="col-md-12">
    <div class="col-md-6">
      <br /><br />
      @include('errors')
    </div>
  </div>


<script>
    $( document ).ready( function(){
        $( '.summerText' ).summernote({
          minHeight: 150,
          toolbar: [
                [ 'style', [ 'style' ] ],
                [ 'font', [ 'bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear'] ],
                [ 'fontname', [ 'fontname' ] ],
                [ 'fontsize', [ 'fontsize' ] ],
                [ 'color', [ 'color' ] ],
                [ 'para', [ 'ol', 'ul', 'paragraph', 'height' ] ],
                [ 'table', [ 'table' ] ],
                [ 'insert', [ 'link'] ],
                [ 'view', [ 'undo', 'redo', 'fullscreen', 'codeview' ] ]
            ]
        });
    } );
        
</script>

<meta name="pages_token" content="{{ csrf_token() }}">



@endsection
