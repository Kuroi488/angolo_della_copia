
<?php
use App\Models\Cover_Type;
if( !isset( $cover ) ){ return view( '404' ); }
$path = App\Models\Cover::updatePath( $cover->id );
?>


@extends( 'office' )

@section( 'office_body' )

  <section>
    <h2>Modifica Copertina</h2>

  <?php echo Form::open(['url' => $path, 'method' => 'post', 'enctype' => 'multipart/form-data', 'class' => 'col-md-6']);
          echo Form::token();
  ?>
  <input type="hidden" name="id" value="<?php echo $cover->id; ?>"/>

          <div class="form-group">
            <?php
              echo Form::label('name', 'Nome:', ['class' => 'control-label']);
              echo Form::text('name', $cover->name, array_merge(['class' => 'form-control']));
            ?>
          </div>

          @if(count($categories)>0)
            <div class="form-group">
              <?php
                echo Form::label('category', 'Categoria:', array('class' => 'control-label'));
                echo '<select id="category2" name="category" class="form-control">';
                echo '<option value="0">Selezionare Categoria</option>';
                foreach( $categories as $category ){
                    $selected = ( $category['id'] == $cover[ 'associated-category' ] ) ? 'selected="selected"' : '';
                  echo '<option value="' . $category['id'] . '"' . $selected . '>' . $category['name'] . '</option>';
                }
                echo '</select>';
              ?>
            </div>
          @endif
          @if( count( $types ) > 0 )
          <div class="form-group">
            <label for="types" class="control-label">Seleziona tipologia:</label>
            <select class="form-control" id="type" name="type">
              <option value="0">Selezionare Tipologia</option>
              <?php
                foreach( $types as $type ){
                    $selected = ( $type['id'] == $cover[ 'associated-type' ] ) ? 'selected="selected"' : '';
                    echo '<option value="' . $type['id'] . '"' . $selected . '>' . $type['name'] . '</option>';
                }
              ?>
            </select>
          </div>
          @endif
          
          <div class="form-group">
            <?php
              echo Form::label('quantity', 'Quantità:', ['class' => 'control-label']);
              echo Form::text('quantity', $cover->quantity, array_merge(['class' => 'form-control']));
            ?>
          </div>

          <div class="form-group">
            <label for="preview" class="control-label col-md-12">Carica preview:</label>
            <img src="{{ asset( 'assets/images/covers/' . $cover[ "background-image" ]) }}" class="coverImages previewThumb col-md-3"/>
            <div class="col-md-12 coverInputs">
              <input type="file" name="preview" id="preview" class="coverImagesInput" value=""/>
              <button class="removeImage btn btn-warning pull-right" >Rimuovi</button>
            </div>
          </div>
          <div class="" style="clear: both; height: 20px; "></div>

          <div class="form-group">
            <label for="icon" class="control-label col-md-12">Carica icona:</label>
            <img src="{{ asset( 'assets/images/covers/' . $cover[ "preview-image" ] ) }}" class="coverImages iconThumb col-md-3"/>
            <div class="col-md-12 coverInputs">
              <input type="file" name="icon" id="icon" class="coverImagesInput" value=""/>
              <button class="removeImage btn btn-warning pull-right" >Rimuovi</button>
            </div>
          </div>

          <div style="clear: both; height: 20px; "></div>

          <button type="submit" class="btn btn-default">Modifica</button>

    <?php echo Form::close(); ?>
  </section>

  <div class="col-md-12">
    <div class="col-md-6">
      <br /><br />
      @include('errors')
    </div>
  </div>

  <meta name="category_token" content="{{ csrf_token() }}">

@endsection
