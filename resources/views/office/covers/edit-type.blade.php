
<?php
if( !isset( $data ) ){ return view( '404' ); }
$categories = $data[ 'categories' ];
$cover_type = $data[ 'cover_type' ];
use App\Models\Cover_Type;
$path = App\Models\Cover_Type::updatePath( $cover_type->id );
?>

@extends( 'office' )

@section( 'office_body' )

  <section>
    <h2>Crea Tipologia</h2>

  <?php echo Form::open(['url' => $path, 'method' => 'get', 'class' => 'col-md-6']);
          echo Form::token(); ?>
          <input type="hidden" name="id" value="<?php echo $cover_type->id; ?>">

          <div class="form-group">
            <?php
              echo Form::label('name', 'Nome:', ['class' => 'control-label']);
              echo Form::text('name', $cover_type->name, array_merge(['class' => 'form-control']));
            ?>
          </div>

          @if(count($categories)>0)
            <div class="form-group">
              <?php
                echo Form::label('category', 'Categoria:', array('class' => 'control-label'));
                echo '<select id="category" name="category" class="form-control">';
                echo '<option value="0">Selezionare Categoria</option>';
                foreach( $categories as $category ){
                  $selected = ( $category['id'] == $cover_type[ 'associated-category' ] ) ? 'selected="selected"' : '';
                  echo '<option value="' . $category['id'] . '"' . $selected . '>' . $category['name'] . '</option>';
                }
                echo '</select>';
              ?>
            </div>
          @endif

          <button type="submit" class="btn btn-default">Modifica</button>

    <?php echo Form::close(); ?>
  </section>

  <div class="col-md-12">
    <div class="col-md-6">
      <br /><br />
      @include('errors')
    </div>
  </div>

@endsection
