
<?php
use App\Models\Cover_Category;
$path = App\Models\Cover_Category::newPath();
?>

@extends( 'office' )

@section( 'office_body' )

  <section>
    <h2>Crea Categoria</h2>

  <?php echo Form::open(['url' => $path, 'method' => 'POST', 'class' => 'col-md-6']);
          echo Form::token(); ?>

          <div class="form-group">
            <?php
              echo Form::label('name', 'Nome:', ['class' => 'control-label']);
              echo Form::text('name', null, array_merge(['class' => 'form-control']));
            ?>
          </div>
      @if(count($countries)>0)
          <div class="form-group">
            <?php
              echo Form::label('country', 'Seleziona Paese:', array('class' => 'control-label'));
              echo '<select id="country" name="country" class="form-control">';
              echo '<option value="0">Selezionare Paese</option>';
              foreach( $countries as $country ){
                echo '<option value="' . $country['id'] . '">' . $country['name'] . '</option>';
              }
              echo '</select>';
            ?>
          </div>
        @endif
        <div class="form-group">
          <label for="language" class="control-label">Seleziona lingua:</label>
          <select class="form-control" id="language" name="language" disabled="disabled">
            <option value="" >Selezionare lingua</option>
          </select>
        </div>
          <div class="form-group">
            <?php
              echo Form::label('description', 'Descrizione:', ['class' => 'control-label']);
              echo Form::textarea('description', null, array_merge(['class' => 'form-control summerText']));
            ?>
          </div>
          <div class="form-group">
            <?php
              echo Form::label('cost', 'Costo:', ['class' => 'control-label']);
              echo Form::text('cost', null, array_merge(['class' => 'form-control']));
            ?>
          </div>
          <button type="submit" class="btn btn-default">Crea</button>

    <?php echo Form::close(); ?>
  </section>

  <div class="col-md-12">
    <div class="col-md-6">
      <br /><br />
      @include('errors')
    </div>
  </div>

<script>
    $( document ).ready( function(){
        $( '.summerText' ).summernote({
          minHeight: 150,
          toolbar: [
                [ 'style', [ 'style' ] ],
                [ 'font', [ 'bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear'] ],
                [ 'fontname', [ 'fontname' ] ],
                [ 'fontsize', [ 'fontsize' ] ],
                [ 'color', [ 'color' ] ],
                [ 'para', [ 'ol', 'ul', 'paragraph', 'height' ] ],
                [ 'table', [ 'table' ] ],
                [ 'insert', [ 'link'] ],
                [ 'view', [ 'undo', 'redo', 'fullscreen', 'codeview' ] ]
            ]
        });
    } );
        
</script>


<meta name="pages_token" content="{{ csrf_token() }}">

@endsection
