
<?php
use App\Models\Country;
use App\Models\Language;
$path = App\Models\Language::updatePath( $language->id );
?>

@extends( 'office' )

@section( 'office_body' )

  <section>
    <h2>Modifica Lingua</h2>

  <?php echo Form::open(['url' => $path, 'method' => 'get', 'class' => 'col-md-6']);
          echo Form::token(); ?>

          @if(count($countries)>0)
            <div class="form-group">
              <?php
                echo Form::label('country', 'Seleziona Paese:', array('class' => 'control-label'));
                echo Form::select('country', $countries, $language->countryID, array_merge(['class' => 'form-control'], [ 'value' => $language->countryID ] ) );
                ?>
            </div>
          @endif

          <div class="form-group">
            <?php
              echo Form::label('name', 'Nome:', ['class' => 'control-label']);
              echo Form::text('name', $language->name, array_merge(['class' => 'form-control']));
            ?>
          </div>

          @include( 'publishing_checkbox', [ 'variable' => $language ] )

          <button type="submit" class="btn btn-default">Modifica</button>

    <?php echo Form::close(); ?>
  </section>

  <div class="col-md-12">
    <div class="col-md-6">
      <br /><br />
      @include('errors')
    </div>
  </div>

@endsection
