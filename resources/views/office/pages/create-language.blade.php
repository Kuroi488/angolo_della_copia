
<?php
use App\Models\Country;
$path = App\Models\Language::newPath();
?>

@extends( 'office' )

@section( 'office_body' )

  <section>
    <h2>Crea Lingua</h2>

  <?php echo Form::open(['url' => $path, 'method' => 'get', 'class' => 'col-md-6']);
          echo Form::token(); ?>

          @if(count($countries)>0)
            <div class="form-group">
              <?php
                echo Form::label('country', 'Seleziona Paese:', array('class' => 'control-label'));
                echo '<select id="country" name="country" class="form-control">';
                echo '<option value="0">Selezionare Paese</option>';
                foreach( $countries as $country ){
                  echo '<option value="' . $country['id'] . '">' . $country['name'] . '</option>';
                }
                echo '</select>';
              ?>
            </div>
          @endif

          <div class="form-group">
            <?php
              echo Form::label('name', 'Nome:', ['class' => 'control-label']);
              echo Form::text('name', null, array_merge(['class' => 'form-control']));
            ?>
          </div>
          <button type="submit" class="btn btn-default">Crea</button>

    <?php echo Form::close(); ?>
  </section>

  <div class="col-md-12">
    <div class="col-md-6">
      <br /><br />
      @include('errors')
    </div>
  </div>

@endsection
