
<?php
use App\Models\Country;
$path = App\Models\Country::newPath();
?>

@extends( 'office' )

@section( 'office_body' )

  <section>
    <h2>Modifica Paese</h2>

  <?php echo Form::open(['url' => $path, 'method' => 'get', 'class' => 'col-md-6']);
          echo Form::token(); ?>

          <div class="form-group">
            <?php
              echo Form::label('name', 'Nome:', ['class' => 'control-label']);
              echo Form::text('name', null, array_merge(['class' => 'form-control']));
            ?>
          </div>
          <button type="submit" class="btn btn-default">Crea</button>

    <?php echo Form::close(); ?>
  </section>

  <div class="col-md-12">
    <div class="col-md-6">
      <br /><br />
      @include('errors')
    </div>
  </div>

@endsection
