
<?php
  use App\Models\Country;
  use App\Models\Language;
  $path = App\Models\Pages::newPath();
?>


@extends( 'office' )

@section( 'office_body' )

  <div class="col-md-12 errorsWrapper" style="display: none;">
    <div class="col-md-6">
      <div class="alert alert-danger">
          <ul>
          </ul>
      </div>
      <br /><br />
    </div>
  </div>

  <section>
    <div class="col-md-8">
      <h2>Crea Pagina</h2>

      <?php
      echo Form::open(['class' => 'col-md-10 createPageForm']); ?>

        <input type="hidden" name="route" value="<?php echo $path; ?>">

        @if(count($countries)>0)
          <div class="form-group">
            <?php
              echo Form::label('country', 'Seleziona Paese:', array('class' => 'control-label'));
              echo '<select id="country" name="country" class="form-control">';
              echo '<option value="0">Selezionare Paese</option>';
              foreach( $countries as $country ){
                echo '<option value="' . $country['id'] . '">' . $country['name'] . '</option>';
              }
              echo '</select>';
            ?>
          </div>
        @endif
        <div class="form-group">
          <label for="language" class="control-label">Seleziona lingua:</label>
          <select class="form-control" id="language" name="language" disabled="disabled">
            <option value="" >Selezionare lingua</option>
          </select>
        </div>

        <div class="form-group">
          <?php
            echo Form::label('name', 'Nome:', ['class' => 'control-label']);
            echo Form::text('name', null, array_merge(['class' => 'form-control']));
          ?>
        </div>

        <div class="form-group meta-tags-filler">
          <?php echo Form::label('meta-tag', 'Meta tags:', ['class' => 'control-label']); ?>
          <button class="btn btn-success" type="button" name="button" onclick="addTAG( $(this) )">
            <i class="fa fa-plus" aria-hidden="true"></i>
          </button>
          <br>
          <hr style="width: 100%; border: solid 1px; margin-bottom: 20px;">
          <ul class="">
            <li>
              <?php echo Form::label('meta-tag-name', 'Name:', ['class' => 'control-label']); ?>
              <?php echo Form::label('meta-tag-content', 'Content:', ['class' => 'control-label']); ?>
              <br>
              <?php echo Form::text('meta-tag-name', null, array_merge(['class' => 'form-control meta'])); ?>
              <?php echo Form::text('meta-tag-content', null, array_merge(['class' => 'form-control meta'])); ?>
              <button class="btn btn-danger" type="button" name="button" onclick="removeTAG( $(this) )">
                <i class="fa fa-remove" aria-hidden="true"></i>
              </button>
            </li>
          </ul>

        </div>

        <br>

        <div class="form-group">
          <label for="components"> Inserisci qui i Componenti: </label>
          <ul id="sortable2" class="connectedSortable">
          </ul>
        </div>

        <button type="submit" class="btn btn-default submitCreatePage" onclick="sumbitForm(event)">Crea</button>
      <?php echo Form::close(); ?>
    </div>

    <div class="col-md-4">
      <h2 class="text-left" >Lista componenti</h2>
      @if(count($components)>0)
      <ul id="sortable1" class="connectedSortable">

        <?php
          foreach( $components as $component ):
        ?>
            <li class="ui-state-default">
              <span class="componentName"> <?php echo $component['pageName']; ?> </span>
              <button type="button" name="button" class="btn btn-warning deleteComponentButton" onclick="removeTAG( $(this) )">
                <i class="fa fa-trash-o" aria-hidden="true"></i>
              </button>
              <br>
              <?php foreach ($component['elements'] as $element): ?>
                <label class="markerLabel" for="<?php echo $element; ?>" > <?php echo $element; ?>: </label>
                <textarea name="<?php echo $element;?>"
                          id="<?php echo $element; ?>"
                          class="form-control note-editable panel-body">Inserisci il contenuto</textarea>
              <?php endforeach; ?>
            </li>
        <?php
          endforeach;
        ?>
      </ul>
        @endif
    </div>

  </section>

  <meta name="pages_token" content="{{ csrf_token() }}">

@endsection
