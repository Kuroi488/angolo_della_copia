
@extends( 'office' )

@section( 'office_body' )

<section class="pagesIndex">

  <div class="col-md-6 col-xs-12">
    <?php foreach ($countries as $country ): ?>
      <div class="country col-md-12">
        <b><?php echo $country->name; ?></b>
        <a data-country="<?php echo $country->id; ?>"><i class="fa fa-arrow-circle-down switchAncor" aria-hidden="true"></i></a>
        <!-- <input type="checkbox" name="" value=""> -->
        <a href="<?php echo App\Models\Country::deletePath( $country->id ); ?>" onclick="warning( event );">elimina</a>
        <a href="<?php echo App\Models\Country::editPath( $country->id ); ?>">modifica</a>
      </div>
      <?php //languages loop
      $languages =  App\Models\Country::languages( $country->id );
        if( $languages ){
          foreach ( $languages as $language ) { ?>
            <div class="language col-md-12" data-country="<?php echo $country->id; ?>">
              <b><?php echo $language->name; ?></b>
              <a data-language="<?php echo $language->id; ?>"><i class="fa fa-arrow-circle-down switchAncor" aria-hidden="true"></i></a>
              <!-- <input type="checkbox" name="" value=""> -->
              <a href="<?php echo App\Models\Language::deletePath( $language->id ); ?>" onclick="warning( event );">elimina</a>
              <a href="<?php echo App\Models\Language::editPath( $language->id ); ?>">modifica</a>
            </div>
            <?php // pages loop
            $pages = App\Models\Language::pages( $language->id, $country->id );
            if( $pages ){
              foreach ( $pages as $page ) { ?>
                <div class="page col-md-12" data-language="<?php echo $language->id; ?>">
                  <i><?php echo $page->permalink; ?></i>
                  <!-- <input type="checkbox" name="" value=""> -->
                  <a href="<?php echo App\Models\Pages::deletePath( $page->id ); ?>" onclick="warning( event );">elimina</a>
                  <a href="<?php echo App\Models\Pages::editPath( $page->id ); ?>">modifica</a>
                </div>
              <?php }
            }
            ?>
        <?php  }
         }
         /* languages loop */ ?>
    <?php endforeach; ?>

  </div>

  <div class="col-md-5 col-xs-12 col-md-offset-1">
    <div class="col-md-12 massiveTitle"> Gestione </div>
    <div class="col-md-6 massiveInput"><a>pubblica</a></div>
    <div class="col-md-6 massiveInput"><a>nascondi</a></div>
    <div class="col-md-12 massiveTitle"> Creazione </div>
    <div class="col-md-4 create create-country"><a href="<?php echo App\Models\Country::createPath(); ?>">Crea Nazione</a></div>
    <div class="col-md-4 create create-language"><a href="<?php echo App\Models\Language::createPath(); ?>">Crea Lingua</a></div>
    <div class="col-md-4 create create-page"><a href="<?php echo App\Models\Pages::createPath(); ?>">Crea Pagina</a></div>
  </div>

  <div class="col-md-12">
      <br /><br />
      @include('errors')
  </div>

</section>

<div class="col-md-12">
  <div class="col-md-6">
    <br /><br />
    @include('errors')
  </div>
</div>

<script type="text/javascript">
  warning = function( event ){
    var q = confirm('Are you sure you want to remove this item?');
    if( !q ){
      event.preventDefault();
      return false;
    }
  }
</script>


@endsection
