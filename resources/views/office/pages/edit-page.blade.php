
<?php
  use App\Models\Country;
  use App\Models\Language;
  $path = App\Models\Pages::updatePath( $page->id );
?>


@extends( 'office' )

@section( 'office_body' )

  <div class="col-md-12 errorsWrapper" style="display: none;">
    <div class="col-md-6">
      <div class="alert alert-danger">
          <ul>
          </ul>
      </div>
      <br /><br />
    </div>
  </div>

  <section>
    <div class="col-md-8">
      <h2>Crea Pagina</h2>

      <?php
      echo Form::open(['class' => 'col-md-10 createPageForm']); ?>

              <input type="hidden" name="oldPermalink" value="<?php echo $page->permalink; ?>">
              <input type="hidden" name="route" value="<?php echo $path; ?>">

        @if(count($countries)>0)
          <div class="form-group">
            <?php
              echo Form::label('country', 'Seleziona Paese:', array('class' => 'control-label'));
              echo '<select id="country" name="country" class="form-control">';
              echo '<option value="0">Selezionare Paese</option>';
              foreach( $countries as $country ){
                $selected = ( $country['id'] == $page->country ) ? 'selected="selected"' : '';
                echo '<option value="' . $country['id'] . '"' . $selected . '>' . $country['name'] . '</option>';
              }
              echo '</select>';
            ?>
          </div>
        @endif
        <div class="form-group">
          <label for="language" class="control-label">Seleziona lingua:</label>
          <select class="form-control" id="language" name="language">
            <option value="0">Selezionare Lingua</option>
            <?php
              foreach( $languages as $language ){
                if( $language['countryID'] == $page->country ){
                  $selected = ( $language['id'] == $page->language ) ? 'selected="selected"' : '';
                  echo '<option value="' . $language['id'] . '"' . $selected . '>' . $language['name'] . '</option>';
                }
              }
            ?>
          </select>
        </div>

        <div class="form-group">
          <?php
            $permalinkArray = explode( "/", $page->permalink );
            $oldName = str_replace( $permalinkArray[0] . '/', '', $page->permalink );
            $oldName = str_replace( $permalinkArray[1] . '/', '', $oldName );
            echo Form::label('name', 'Nome:', ['class' => 'control-label']);
            echo Form::text('name', $oldName, array_merge(['class' => 'form-control']));
          ?>
        </div>

        @include( 'publishing_checkbox', [ 'variable' => $page ] )

        <div class="form-group meta-tags-filler">
          <?php
            echo Form::label('meta-tag', 'Meta tags:', ['class' => 'control-label']);
          ?>
          <button class="btn btn-success" type="button" name="button" onclick="addTAG( $(this) )">
            <i class="fa fa-plus" aria-hidden="true"></i>
          </button>
          <br>
          <hr style="width: 100%; border: solid 1px; margin-bottom: 20px;">
          <ul class="">
            <li>
              <?php
                $labels = '<label for="meta-tag-name" class="control-label">Name:</label><label for="meta-tag-content" class="control-label">Content:</label><br>';
                $button =  '<button class="btn btn-danger" type="button" name="button" onclick="">';
                $button .=  '<i class="fa fa-remove" aria-hidden="true"></i>';
                $button .= '</button>';
                foreach( $metas as $meta ){
                  $name = strstr( $meta['name'], '@#', true );
                  echo $labels;
                  echo '<input class="form-control meta" name="meta-tag-name" type="text" id="meta-tag-name" value="' . $name . '">';
                  echo '<input class="form-control meta" name="meta-tag-content" type="text" id="meta-tag-content" value="' . $meta['body'] . '">';
                  echo $button;
                }
              ?>
            </li>
          </ul>

        </div>

        <br>

        <div class="form-group">
          <label for="components"> Inserisci qui i Componenti: </label>
          <ul id="sortable2" class="connectedSortable">
            <?php
              foreach( $usedComponents as $component => $content ){
                $box = '<li class="ui-state-default ui-sortable-handle">';
                $box .=  '<span class="componentName">' . strstr( $component, "@#", true ) .  '</span>';
                $box .=    '<button type="button" name="button" class="btn btn-warning deleteComponentButton" onclick="removeTAG( $(this) )">';
                $box .=      '<i class="fa fa-trash-o" aria-hidden="true"></i>';
                $box .=    '</button>';
                $box .=    '<br>';
                foreach( $content as $text ){
                  if( $text['marker'] != 'null' ){
                    $box .=    '<label class="markerLabel" for="' . $text['marker'] . '">' . $text['marker'] . ':</label>';
                    $box .=    '<textarea name="' . $text['marker'] . '" id="' . $text['marker'] . '" class="form-control note-editable panel-body" ondrop="">' . $text['body'] . '</textarea>';
                  }
                }
                $box .= '</li>';
                echo $box;
              }
            ?>
          </ul>
        </div>

        <button type="submit" class="btn btn-default submitCreatePage" onclick="sumbitForm(event)">Modifica</button>
      <?php echo Form::close(); ?>
    </div>

    <div class="col-md-4">
      <h2 class="text-left" >Lista componenti</h2>
      @if(count($components)>0)
      <ul id="sortable1" class="connectedSortable">

        <?php
          foreach( $components as $component ):
        ?>
            <li class="ui-state-default">
              <span class="componentName"> <?php echo $component['pageName']; ?> </span>
              <button type="button" name="button" class="btn btn-warning deleteComponentButton" onclick="removeTAG( $(this) )">
                <i class="fa fa-trash-o" aria-hidden="true"></i>
              </button>
              <br>
              <?php foreach ($component['elements'] as $element): ?>
                <label class="markerLabel" for="<?php echo $element; ?>" > <?php echo $element; ?>: </label>
                <textarea name="<?php echo $element;?>"
                          id="<?php echo $element; ?>"
                          class="form-control note-editable panel-body">Inserisci il contenuto</textarea>
              <?php endforeach; ?>
            </li>
        <?php
          endforeach;
        ?>
      </ul>
        @endif
    </div>

  </section>

  <meta name="pages_token" content="{{ csrf_token() }}">

@endsection
