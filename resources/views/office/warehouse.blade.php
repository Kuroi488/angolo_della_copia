@extends( 'office' )

@section( 'office_body' )
<div class="row materials">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail text-center">
      <i class="fa fa-book fa-5x" aria-hidden="true"></i>
      <div class="caption">
        <h3>Book bindings</h3>
        <p>Manage book bindings type</p>
        <p><a href="/office/warehouse/covers/index" class="btn btn-primary" role="button">Open</a></p>
      </div>
    </div>
  </div>
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail text-center">
      <i class="fa fa-paint-brush fa-5x" aria-hidden="true"></i>
      <div class="caption">
        <h3>Colors</h3>
        <p>Manage print colors</p>
        <p><a href="/office/materials/colors/index" class="btn btn-primary" role="button">Open</a></p>
      </div>
    </div>
  </div>
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail text-center">
      <i class="fa fa-files-o fa-5x" aria-hidden="true"></i>
      <div class="caption">
        <h3>Data-entry</h3>
        <p>Manage weights of paper</p>
        <p><a href="/office/warehouse/papers/index" class="btn btn-primary" role="button">Open</a></p>
      </div>
    </div>
  </div>
</div>


@endsection
