
<?php
  use App\Models\Country;
  use App\Models\Language;
  $path = App\Models\Paper::updatePath( $paper->id );
?>


@extends( 'office' )

@section( 'office_body' )

  <section>
    
   
    
    <div class="col-md-8">
      <h2>Modifica Grammatura</h2>

      <?php
      echo Form::open(['class' => 'col-md-10', 'method' => 'POST', 'url' => $path]); ?>

        @if(count($countries)>0)
          <div class="form-group">
            <?php
              echo Form::label('country', 'Seleziona Paese:', array('class' => 'control-label'));
              echo '<select id="country" name="country" class="form-control">';
              echo '<option value="0">Selezionare Paese</option>';
              
              foreach( $countries as $country ){
                $selected = ( $country['id'] == $paper[ 'associated-country' ] ) ? 'selected="selected"' : '';
                echo '<option value="' . $country['id'] . '"' . $selected . '>' . $country['name'] . '</option>';
              }
              echo '</select>';
            ?>
          </div>
        @endif
        <div class="form-group">
          <label for="language" class="control-label">Seleziona lingua:</label>
          <select class="form-control" id="language" name="language">
            <option value="0">Selezionare Lingua</option>
            <?php
              foreach( $languages as $language ){
                if( $language['countryID'] == $paper[ 'associated-language' ] ){
                  $selected = ( $language['id'] == $paper[ 'associated-language' ] ) ? 'selected="selected"' : '';
                  echo '<option value="' . $language['id'] . '"' . $selected . '>' . $language['name'] . '</option>';
                }
              }
            ?>
          </select>
        </div>

        <div class="form-group">
          <?php
            echo Form::label('name', 'Nome:', ['class' => 'control-label']);
            echo Form::text('name', $paper->name, array_merge(['class' => 'form-control']));
          ?>
        </div>
        
        <div class="form-group">
          <?php
            echo Form::label('weight', 'Peso:', ['class' => 'control-label']);
            echo Form::text('weight', $paper->weight, array_merge(['class' => 'form-control']));
          ?>
        </div>
        
        <div class="form-group">
          <?php
            echo Form::label('cost', 'Costo:', ['class' => 'control-label']);
            echo Form::text('cost', $paper->cost, array_merge(['class' => 'form-control']));
          ?>
        </div>
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <button type="submit" class="btn btn-default submitCreatePage" >Modifica</button>
      <?php echo Form::close(); ?>
    </div>

  </section>

  <meta name="pages_token" content="{{ csrf_token() }}">

@endsection
