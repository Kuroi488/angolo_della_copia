
@extends( 'office' )

@section( 'office_body' )

<section class="pagesIndex">

  <div class="col-md-6 col-xs-12">
    <?php foreach ( $countries as $country ): ?>
      <div class="country col-md-12">
        <b><?php echo $country->name; ?></b>
        <a data-country="<?php echo $country->id; ?>"><i class="fa fa-arrow-circle-down switchAncor" aria-hidden="true"></i></a>
      </div>
      <?php //languages loop
      $languages =  App\Models\Country::languages( $country->id );
        if( $languages ){
          foreach ( $languages as $language ) { ?>
            <div class="language col-md-12" data-country="<?php echo $country->id; ?>">
              <b><?php echo $language->name; ?></b>
              <a data-language="<?php echo $language->id; ?>"><i class="fa fa-arrow-circle-down switchAncor" aria-hidden="true"></i></a>
            </div>
            <?php // papers loop
            $papers = App\Models\Language::papers( $language->id, $country->id );
            if( $papers ){
              foreach ( $papers as $paper ) { ?>
                <div class="page col-md-12" data-language="<?php echo $language->id; ?>">
                  <i><?php echo $paper->name; ?></i>
                  <a href="<?php echo App\Models\Paper::deletePath( $paper->id ); ?>" onclick="warning( event );">elimina</a>
                  <a href="<?php echo App\Models\Paper::editPath( $paper->id ); ?>">modifica</a>
                </div>
              <?php }
            }
            ?>
        <?php  }
         }
         /* languages loop */ ?>
    <?php endforeach; ?>

  </div>

  <div class="col-md-5 col-xs-12 col-md-offset-1">
    <div class="col-md-12 massiveTitle"> Gestione </div>
    
    <div class="col-md-12 text-center"><a class="pull-left" href="<?php echo App\Models\Paper::createPath(); ?>">Crea Grammatura</a></div>
  </div>

  <div class="col-md-12">
      <br /><br />
      @include('errors')
  </div>

</section>

<script type="text/javascript">
  warning = function( event ){
    var q = confirm('Are you sure you want to remove this item?');
    if( !q ){
      event.preventDefault();
      return false;
    }
  }
</script>


@endsection
