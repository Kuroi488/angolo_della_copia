
<?php
  use App\Models\Country;
  $path = App\Models\Paper::newPath();
?>


@extends( 'office' )

@section( 'office_body' )

  <div class="col-md-12 errorsWrapper" style="display: none;">
    <div class="col-md-6">
      <div class="alert alert-danger">
          <ul>
          </ul>
      </div>
      <br /><br />
    </div>
  </div>

  <section>
    <div class="col-md-8">
      <h2>Crea Grammatura</h2>

      <?php
      echo Form::open(['class' => 'col-md-10 createPageForm', 'method' => 'GET', 'url' => $path]); ?>

        @if(count($countries)>0)
          <div class="form-group">
            <?php
              echo Form::label('country', 'Seleziona Paese:', array('class' => 'control-label'));
              echo '<select id="country" name="country" class="form-control">';
              echo '<option value="0">Selezionare Paese</option>';
              foreach( $countries as $country ){
                echo '<option value="' . $country['id'] . '">' . $country['name'] . '</option>';
              }
              echo '</select>';
            ?>
          </div>
        @endif
        <div class="form-group">
          <label for="language" class="control-label">Seleziona lingua:</label>
          <select class="form-control" id="language" name="language" disabled="disabled">
            <option value="" >Selezionare lingua</option>
          </select>
        </div>

        <div class="form-group">
          <?php
            echo Form::label('name', 'Nome:', ['class' => 'control-label']);
            echo Form::text('name', null, array_merge(['class' => 'form-control']));
          ?>
        </div>
        
        <div class="form-group">
          <?php
            echo Form::label('weight', 'Peso:', ['class' => 'control-label']);
            echo Form::text('weight', null, array_merge(['class' => 'form-control']));
          ?>
        </div>
        
        <div class="form-group">
          <?php
            echo Form::label('cost', 'Costo:', ['class' => 'control-label']);
            echo Form::text('cost', null, array_merge(['class' => 'form-control']));
          ?>
        </div>

        <button type="submit" class="btn btn-default submitCreatePage" >Crea</button>
        
        <meta name="pages_token" content="{{ csrf_token() }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        
      <?php echo Form::close(); ?>
    </div>
    
    <div class="col-md-12">
    <div class="col-md-6">
      <br /><br />
      @include('errors')
    </div>
  </div>
    

  </section>

  

@endsection
