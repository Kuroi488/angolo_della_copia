@extends( 'office' )

@section( 'office_body' )

<?php
  use app\Models\Cover_Category;
  use app\Models\Cover_Type;
  use app\Models\Text_Color;
?>

<section class="covers_index">

  <div class="col-lg-6 col-xs-12">
    <?php foreach ($categories as $category ): ?>
      <div class="category col-md-12">
        <b><?php echo $category->name; ?></b>
      </div>
      <?php //types loop
      $types =  App\Models\Cover_Type::sort( $category->id );

        if( $types ){
          foreach ( $types as $type ) { ?>
            <div class="type col-md-12" data-category="<?php echo $type->id; ?>">
              <b><?php echo $type->name; ?></b>
            </div>
            <?php // pages loop
             $colors = App\Models\Text_Color::colors( $category->id, $type->id );
            if( isset( $colors ) ){
              foreach ( $colors as $color ) { ?>
                <div class="page col-md-12" data-language="<?php echo $color->id; ?>">
                  <i><?php echo $color->name; ?></i>
                  <!-- <input type="checkbox" name="" value=""> -->
                  <a href="<?php echo App\Models\Text_Color::deletePath( $color->id ); ?>" onclick="warning( event );">elimina</a>
                  <a href="<?php echo App\Models\Text_Color::editPath( $color->id ); ?>">modifica</a>
                </div>
              <?php }
            }
            ?>
        <?php  }
         }
         /* languages loop */ ?>
    <?php endforeach; ?>
  </div>

    <div class="col-md-5 col-xs-12 col-md-offset-1">
      <div class="col-md-12 massiveTitle"> Creazione </div>
      <div class="col-md-4 create create-color"><a href="<?php echo App\Models\Text_Color::createPath(); ?>">Crea Colore</a></div>
    </div>

</section>


<div class="col-md-12">
  <div class="col-md-6">
    <br /><br />
    @include('errors')
  </div>
</div>

<script type="text/javascript">
  warning = function( event ){
    var q = confirm('Are you sure you want to remove this item?');
    if( !q ){
      event.preventDefault();
      return false;
    }
  }
</script>


@endsection
