
<?php
use App\Models\Cover_Category;
use App\Models\Text_Color;
$path = App\Models\Text_Color::newPath();
?>

@extends( 'office' )

@section( 'office_body' )

    <section>
        <h2>Crea Colore del Testo</h2>

        <?php echo Form::open(['url' => $path, 'method' => 'post', 'enctype' => 'multipart/form-data', 'class' => 'col-md-6']);
        echo Form::token(); ?>

        <div class="form-group">
            <?php
            echo Form::label('name', 'Nome:', ['class' => 'control-label']);
            echo Form::text('name', null, array_merge(['class' => 'form-control']));
            ?>
        </div>

        @if(count($categories)>0)
            <div class="form-group">
                <?php
                echo Form::label('category', 'Categoria:', array('class' => 'control-label'));
                echo '<select id="category2" name="category" class="form-control">';
                echo '<option value="0">Selezionare Categoria</option>';
                foreach( $categories as $category ){
                    echo '<option value="' . $category['id'] . '">' . $category['name'] . '</option>';
                }
                echo '</select>';
                ?>
            </div>
        @endif

        <div class="form-group">
            <label for="type" class="control-label">Seleziona tipologia:</label>
            <select class="form-control" id="type" name="type" disabled="disabled">
                <option value="" >Selezionare tipologia</option>
            </select>
        </div>

        <div class="form-group">
            <label for="preview" class="control-label col-md-12">Carica preview:</label>
            <img src="{{asset( 'assets/images/no-photo.png' )}}" class="coverImages previewThumb col-md-3"/>
            <div class="col-md-12 coverInputs">
                <input type="file" name="preview" id="preview" class="coverImagesInput"/>
                <button class="removeImage btn btn-warning pull-right" >Rimuovi</button>
            </div>
        </div>
        <div class="" style="clear: both; height: 20px; "></div>

        <div class="form-group">
            <label for="icon" class="control-label col-md-12">Carica icona:</label>
            <img src="{{asset( 'assets/images/no-photo.png' )}}" class="coverImages iconThumb col-md-3"/>
            <div class="col-md-12 coverInputs">
                <input type="file" name="icon" id="icon" class="coverImagesInput"/>
                <button class="removeImage btn btn-warning pull-right" >Rimuovi</button>
            </div>
        </div>

        <div style="clear: both; height: 20px; "></div>

        <button type="submit" class="btn btn-default">Crea</button>

        <?php echo Form::close(); ?>
    </section>

    <div class="col-md-12">
        <div class="col-md-6">
            <br /><br />
            @include('errors')
        </div>
    </div>

    <meta name="category_token" content="{{ csrf_token() }}">

@endsection
