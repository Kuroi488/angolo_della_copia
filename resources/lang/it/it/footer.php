<?php

  return $footer = [

    'who'   => 'chi siamo',
    'FAQS'   => 'domande frequenti',
    'privacy'    => 'privacy',
    'our-shops' => 'i nostri negozi',
    'social_facebook' => '<i class="fa fa-facebook" aria-hidden="true"></i>',
    'social_twitter' => '<i class="fa fa-twitter" aria-hidden="true"></i>',
    'social_g-plus' => '<i class="fa fa-google-plus" aria-hidden="true"></i>',
    'help' => 'contattaci',


    // custom route links
    'links' => [
      'FAQS'    => 'FAQS',
      'our-shops' => 'negozi',
      'social_facebook' => 'https://www.facebook.com',
      'social_twitter' => 'https://www.twitter.com',
      'social_g-plus' => 'https://www.google.com',
      'help' => 'help'
    ]



  ];
