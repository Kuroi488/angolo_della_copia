<?php

  return $menu = [

    'home'   => 'home',
    'back'   => 'indietro',
    'how'    => 'come<br class="hidden-xs"/> funziona',
    'covers' => 'copertine e<br class="hidden-xs hidden-sm"/> rilegature',
    'GAQ'    => 'preventivo<br class="hidden-xs hidden-sm"/> e prezzi',
    'exped'  => 'spedizione<br class="hidden-xs hidden-sm"/> e consegna',
    'help'   => 'chiedici<br class="hidden-md hidden-lg hidden-xs"/> aiuto',
    'print'  => 'stampa ora<br class="hidden-xs hidden-sm"/> la tua tesi',

    // custom route links
    'links' => [
      'how'    => 'home',
      'covers' => 'copertine-e-rilegature',
      'GAQ'    => 'preventivo-e-prezzi',
      'exped'  => 'spedizione-e-consegna',
      'help'   => 'help',
      'print'  => 'stampa-tesi',
    ]



  ];
