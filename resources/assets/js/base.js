
var colorRows = function(){
  var rows = $('tbody tr:visible');
  var counter = rows.length;
  $('tbody tr').removeClass( 'grey' );
  for( i = 0; i < counter; i++ ){
    if( i % 2 == 0 ){
      rows.eq( i + 1 ).addClass( 'grey' );
    }
  }
};


var tableFilter = function(){

  input = $( 'input[type="search"]' );
  rows = $( 'tbody tr' );
  el = rows.find( 'td' );

  input.on( 'keyup', function(){
    val = input.val();
    if( val != '' ){

      rows.hide();
      el.each( function(){
        if( $(this).text().indexOf( val ) != -1 ){
          $(this).closest( 'tr' ).show();
          colorRows();
          // rows.parent().children( 'tr:visible:nth-child(even)' ).css( 'background-color', 'green' );
        }
      } );

    }else{
      rows.show();
      colorRows();
    }



    } );

};


$(document).ready( function(){

  colorRows();
  tableFilter();

  $('table').paging({limit: 5});


  /* hide menu panel for current table */
  $visibleTable = $('table').attr( 'data-table' );
  $( '.panel.panel-default[data-table="' + $visibleTable + '"]' ).hide();
  $( '.panel.panel-default[data-table!="' + $visibleTable + '"]' ).show();
  $('.panel.panel-default[data-edit="multipleUpdate"]').hide();


  /* adding class SELECTED to table rows */
  $( 'table[data-table="covers"] tbody tr' ).on( 'click', function(){
    if( $(this).attr('class') ){


      if( $(this).attr('class').indexOf( 'selected' ) != -1 ){
         $(this).removeClass('selected');
       }else{
         $(this).addClass( 'selected' );
       }


    }else{
      $(this).addClass( 'selected' );
    }

    /* show/hide multiple editor */
    selectedItems = $( 'tr.selected' ).length;
    if( selectedItems > 1 ){
      $('.panel.panel-default[data-edit="multipleUpdate"]').show();
    }else{
      $('.panel.panel-default[data-edit="multipleUpdate"]').hide();
    }

  });


  /* graphic changes */
  setInterval( function(){

    /* basic input limitation */
    $( 'input[name="rgb_code"]' ).numeric( { negative: false, decimal: false } );
    $( 'input[name="in_stock"]' ).numeric( { negative: false, decimal: false } );
    $( 'input[name="price"]' ).numeric( { negative: false, decimalPlaces: 2 } );

  }, 5 );


  /* enable and disable quality mass update */
  $( '.panel.panel-default[data-edit="multipleUpdate"]' ).on( 'click', 'input[type="checkbox"]', function(){
    disable = $(this).siblings( 'select' ).attr( 'disabled' );
    if( !disable ){
      $(this).siblings( 'select' ).attr( 'disabled', 'disabled' );
    }else{
      $(this).siblings( 'select' ).attr( 'disabled', false );
    }

  } );


    /* adding tooltips */
    $('[data-toggle="tooltip"]').tooltip();

    /* bootstrap pagination */


} );
