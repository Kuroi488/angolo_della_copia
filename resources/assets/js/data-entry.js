$( document ).ready( function(){

  // open paging index
  $( 'a[data-country]' ).on( 'click', function(){
	   $id = $(this).attr( 'data-country' );
     $className = $(this).find('i').attr( 'class' );
     if( $('div[data-country="'  + $id + '"]').length > 0 ){
       $('div[data-country="'  + $id + '"]').toggleClass( 'open' );
       if( $className.indexOf( 'fa-arrow-circle-down' ) != -1 ){
         $(this).find('i').switchClass( 'fa-arrow-circle-down', 'fa-arrow-circle-up' );
       }else{
         $(this).find('i').switchClass( 'fa-arrow-circle-up', 'fa-arrow-circle-down' );
         $('div[data-country="'  + $id + '"]').find( 'a i.fa-arrow-circle-up' ).click();
       }
     }
  } );

  $( 'a[data-language]' ).on( 'click', function(){
	   $id = $(this).attr( 'data-language' );
     $className = $(this).find('i').attr( 'class' );
     if( $('div[data-language="'  + $id + '"]').length > 0 ){
       $('div[data-language="'  + $id + '"]').toggleClass( 'open' );
       if( $className.indexOf( 'fa-arrow-circle-down' ) != -1 ){
         $(this).find('i').switchClass( 'fa-arrow-circle-down', 'fa-arrow-circle-up' );
       }else{
         $(this).find('i').switchClass( 'fa-arrow-circle-up', 'fa-arrow-circle-down' );
       }
     }
  } );

  // populate languages list
  function pupulateList( $list ){
    blankOption = '<option>Selezionare lingua</option>';
    listOptions = '';
    for(var key in $list) {
      listOptions += '<option value="' + key +'">' + $list[key] + '</option>';
    }
    options = blankOption + listOptions;
    $( '#language' ).html( options );
    $( '#language' ).attr( 'disabled', false );
  }

  $('#country').on( 'change', function(){
    id = $( this ).val();
    $.ajaxSetup({
      headers: { 'X-CSRF-TOKEN': $('meta[name="pages_token"]').attr('content') }
    });
    $.ajax({
       url: '/office/data-entry/ajax/languages-list-from-country',
       type: 'POST',
       data: { id : id },
       success: function( resp ){
         pupulateList( resp );
       },
       error: function( resp ){
         alert( resp );
       }
    });
  } );

  // populate languages list
  function pupulateTypesList( $list ){
    blankOption = '<option>Selezionare tipologia</option>';
    listOptions = '';
    for(var key in $list) {
      listOptions += '<option value="' + key +'">' + $list[key] + '</option>';
    }
    options = blankOption + listOptions;
    $( '#type' ).html( options );
    $( '#type' ).attr( 'disabled', false );
  }

  $('#category2').on( 'change', function(){
    id = $( this ).val();
    $.ajaxSetup({
      headers: { 'X-CSRF-TOKEN': $('meta[name="category_token"]').attr('content') }
    });
    $.ajax({
       url: '/office/warehouse/ajax/types-list',
       type: 'POST',
       data: { id : id },
       success: function( resp ){
         if( resp != 'err' ){
           console.log( resp );
           pupulateTypesList( resp );
         } else {
           console.log( 'ERR' );
         }
       },
       error: function( resp ){
         console.log( resp.responseText );
       }
    });
  } );

  //meta-tags inputs
  addTAG = function( el ){
    newLi = '<li>';
    newLi +=  '<label for="meta-tag-name" class="control-label">Name:</label><label for="meta-tag-content" class="control-label">Content:</label><br>';
    newLi +=  '<input class="form-control meta" name="meta-tag-name" type="text" id="meta-tag-name">';
    newLi +=  '<input class="form-control meta" name="meta-tag-content" type="text" id="meta-tag-content">';
    newLi +=  '<button class="btn btn-danger" type="button" name="button" onclick="removeTAG( $(this) )">';
    newLi +=    '<i class="fa fa-remove" aria-hidden="true"></i>';
    newLi +=  '</button>';
    newLi += '</li>';

    $( el ).siblings( 'ul' ).append( newLi );
  };
  removeTAG = function( el ){
    $( el ).parent( 'li' ).remove();
  };

  //drag & drop for components
  $( function() {

      $( "#sortable1" ).sortable({
        //  containment: 'parent',
        connectWith: "#sortable2",
        remove: function(e,li) {
          copyHelper= li.item.clone().insertAfter(li.item);
          $(this).sortable('cancel');
          // return li.clone();
        }
      });

      $( '#sortable2' ).sortable({
        cancel: '.note-editable.panel-body, .panel-default, .btn',
        receive: function ( event, ui ){ callSummerNote(); }
      });

    } ); // sortable end

    //text editor
    function callSummerNote(){
      textareas = $('#sortable2').find( '[name*=editoriale]' );
        textareas.summernote({
          minHeight: 150,
          toolbar: [
                [ 'style', [ 'style' ] ],
                [ 'font', [ 'bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear'] ],
                [ 'fontname', [ 'fontname' ] ],
                [ 'fontsize', [ 'fontsize' ] ],
                [ 'color', [ 'color' ] ],
                [ 'para', [ 'ol', 'ul', 'paragraph', 'height' ] ],
                [ 'table', [ 'table' ] ],
                [ 'insert', [ 'link'] ],
                [ 'view', [ 'undo', 'redo', 'fullscreen', 'codeview' ] ]
            ]
        });
      }

  callSummerNote();



  // covers images
  $( '.coverImagesInput' ).on( 'change', function( event ){
    $( this ).closest( '.form-group' ).find( 'img' ).attr( 'src', URL.createObjectURL( event.target.files[0] ) );
  } );

  $( '.removeImage' ).on( 'click', function( event ){
    event.preventDefault();
    $( this ).siblings( 'input' ).attr( 'value', "" );
    $( this ).closest( '.form-group' ).find( 'img' ).attr( 'src', window.location.origin + '/assets/images/no-photo.png' );
  } );

} );

packData = function(){

  //hide errors
  $('.errorsWrapper').css( 'display', 'none' );

  // get page basic info
  countryID = $('[name="country"]').val();
  languageID = $('[name="language"]').val();
  name = $( '[name="name"]' ).val();

  //meta tags
  metaTAGS = {};
  metas = $( '.meta-tags-filler' ).find( 'input[name="meta-tag-content"]' );
  for( i = 0; i < metas.length; i++ ){
    metaKey = $( metas[i] ).siblings( 'input[name="meta-tag-name"]' ).val();
    metaTAGS[ metaKey + '@#' + i ] = $( metas[ i ] ).val();
  }

  // components
  components = {};
  componentsList = $( '#sortable2' ).find( '.ui-state-default.ui-sortable-handle' );
  componentsList.each( function(){
    componentName = $(this).find( '.componentName' ).text();
    componentIndex = $(this).index();
    labels = $(this).find('.markerLabel');
    markers = {};
    labels.each( function(){
      content = true;
      if( $(this).next('textarea').attr( 'id' ).indexOf( 'editoriale' ) != -1 ){
        content = $(this).next('textarea').next( '.note-editor.note-frame.panel.panel-default' ).find( '.note-editable.panel-body' ).html();
      }else{
        content = $(this).next( 'textarea' ).val();
      }
      markerName = $(this).attr( 'for' );
      markers[markerName] = content;
    } );
    components[ componentName + '@#' + componentIndex ] = markers;
    components[ componentName + '@#' + componentIndex ].index = componentIndex;
  } );

  datas = {};

  if( $('input[name="oldPermalink"]') ){
    datas.oldPermalink = $('input[name="oldPermalink"]').val();
  }
  if( $('input[name="visible"]').is(':checked') ){
    datas.visibility = $('input[name="visible"]').val();
  }

  datas.countryID  = countryID;
  datas.languageID = languageID;
  datas.name       = name;
  datas.metaTAGS   = metaTAGS;
  datas.components = components;
  return datas;
};


//form submitting
function sumbitForm( event ){
  event.preventDefault();

    datas = packData();
    url = $('input[name="route"]').val();

    $.ajaxSetup({
      headers: { 'X-CSRF-TOKEN': $('meta[name="pages_token"]').attr('content'),
                'Content-Type': 'application/x-www-form-urlencoded'
              }
    });
    $.ajax({
       url: url,
       type: 'POST',
       data: datas,
       success: function( resp, status ){
        console.log( resp );
        if( status == 'success' ){
          if( resp === 'ok' ){
            window.location.href = '/office/data-entry/index';
          }else{
            console.log( resp );
            $('.errorsWrapper').find( 'li' ).remove();
            for( var key in resp ){
              $('.errorsWrapper').find( 'ul' ).append( '<li>' + resp[key] + '</li>' );
            }
            $('.errorsWrapper').css( 'display', 'inline-block' );
          }
        }

       },
       error: function( resp ){
         console.log( resp.responseText );
       }
    });
  }
